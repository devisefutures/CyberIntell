# CyberIntell

CyberIntell é uma implementação de uma feed e aplicação capaz de recolher dados e monitorizar/alertar para a ocorrência de dados/ameaças
no [pastebin](http://pastebin.com/) e [twitter](https://twitter.com/).

Está organizada em pequenas aplicações de acordo com o *Threat Intelligence Life Cycle*:
+ Gather - aplicação que vai buscar os dados de acordo com as pesquisas de dados/ameaças a efectuar;
+ Process - aplicação que processa e normaliza os dados obtidos na fase anterior;
+ Create Meaning - aplicação que pega nos dados processados e os filtra e agrupa/separa, de modo a serem mais facilmente analisados pelos analistas;
+ Consumption - aplicação que gera informação adicional, de modo a que os dados obtidos na fase anterior possam ser apresentados aos analistas num ambiente ou aplicação de visualização.

O objetivo é poder ser instalada numa plataforma extremamente low-cost (máquina 4-core ARMv7 CPU com 2GB de RAM
e 50 GB SSD) que possa ser utilizada pelas PME e micro-empresas, assim como em plataforma baseada numa [stack ELK][elk].

A versão 1.0 do CyberIntell foi desenvolvida por José Pina Miranda (jose.miranda@devisefutures.com) e Pedro Queirós (me@pedroqueiros.net),
no âmbito da disciplina de **Cyber Intelligence e Consciência Situacional no Ciberespaço** da
**[Pós-Graduação em Cibersegurança e Ciberdefesa][pg_cscd]** (2016/17)

[elk]: https://www.elastic.co/webinars/introduction-elk-stack "stack ELK"
[pg_cscd]: http://academiamilitar.pt/pos-graduacao-em-ciberseguranca-e-ciberdefesa.html "Pós-Graduação em Cibersegurança e Ciberdefesa"

## 1. Instalação

Copie a última versão da branch master para o seu computador.
De seguida, aceda à diretoria Twitter e efectue o seguinte comando:
```
composer install
```

Para configuração e utilização, aceda ao ficheiro README.md dentro de cada sub-diretoria ([PasteBin][CI_pastebin] e [Twitter][CI_twitter]),
onde encontrará ainda um manual técnico e uma análise critica sobre as componentes do sistema.

[CI_pastebin]: https://gitlab.com/devisefutures/CyberIntell/tree/master/PasteBin "PasteBin"
[CI_twitter]: https://gitlab.com/devisefutures/CyberIntell/tree/master/Twitter "Twitter"

### 1.1 Dependências

O código foi testado em _ambiente Linux e MacOS_, com as seguintes versões de software:
+ **composer**, versão 1.1.0 ou superior,
+ **php**, versão 5.6.30 ou superior,
+ **bash**, versão 3.2.57 ou superior.


## 2. Copyright e Licença

Copyright (c) 2017

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
