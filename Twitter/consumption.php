<?php
/**
* CyberIntell - feed e aplicação capaz de recolher dados e monitorizar/alertar
*              a ocorrência de dados/ameaças.
*
* consumption.php
*     Gera o html necessário para a página de visualização de topo associada a vários ficheiros de
*     gathering, assim como o html necessário para ver o resultado de cada gathering, para GUI
*     baseado em csv-to-html-table e huge.
*
* @link   https://gitlab.com/devisefutures/CyberIntell
*
* @author       José Pina Miranda - Devise Futures, Lda. (jose.miranda@devisefutures.com)
* @copyright    2017, Devise Futures Lda.
* @version      0.5
* @package      CyberIntell
* @subpackage   Twitter
* @since        0.5
* @license      https://gitlab.com/devisefutures/CyberIntell/blob/master/LICENSE.md  GNU General Public License v3.0
*/

# ----- config init -----
/**
 /**
  * @since 0.5
  * @var string GENERAL_SECTION  Nome da secção geral na CONFIG
  */
const GENERAL_SECTION = "general";
  /**
   * @since 0.5
   * @var string HEADER Informação a colocar no header, na GENERAL_SECTION
   */
const HEADER = "header";
  /**
   * @since 0.5
   * @var string DESCRIPTION Descrição da pesquisa na GENERAL_SECTION
   */
const DESCRIPTION = "description";
/**
 * @since 0.6
 * @var array $GLOBALS['VIS_DIRECTIVES'] Diretivas de visualização associadas ao URL de pesquisa.
 *      Definida como variavel global para conpatibilidade com versões php <= 5.6
 */
$GLOBALS['VIS_DIRECTIVES'] = [
    "https://api.twitter.com/1.1/search/tweets.json" => [
                                        "head" => "Termos de pesquisa nos tweets:",
                                        "func" => 'searchtweets'
                                                        ],
    "https://api.twitter.com/1.1/favorites/list.json" => [
                                        "head" => "Lista de tweets favoritos de:",
                                        "func" => 'favoriteslist'
                                                        ],
    "https://api.twitter.com/1.1/followers/list.json" => [
                                        "head" => "Lista de seguidores de:",
                                        "func" => 'favoriteslist'
                                                        ],
    "https://api.twitter.com/1.1/friends/list.json" => [
                                        "head" => "Lista de seguidos por:",
                                        "func" => 'favoriteslist'
                                                       ],
    "https://api.twitter.com/1.1/lists/list.json" => [
                                        "head" => "Listas subscritas por (incluindo as do próprio):",
                                        "func" => 'favoriteslist'
                                                       ],
    "https://api.twitter.com/1.1/lists/members.json" => [
                                        "head" => "Membros da lista:",
                                        "func" => 'listmembers'
                                                       ],
    "https://api.twitter.com/1.1/lists/memberships.json" => [
                                        "head" => "Listas de que é membro:",
                                        "func" => 'favoriteslist'
                                                       ],
    "https://api.twitter.com/1.1/lists/ownerships.json" => [
                                        "head" => "Listas detidas por:",
                                        "func" => 'favoriteslist'
                                                       ],
    "https://api.twitter.com/1.1/lists/show.json" => [
                                        "head" => "Informação da lista:",
                                        "func" => 'listmembers'
                                                       ],
    "https://api.twitter.com/1.1/lists/statuses.json" => [
                                        "head" => "Tweets na lista:",
                                        "func" => 'listmembers'
                                                       ],
    "https://api.twitter.com/1.1/lists/subscribers.json" => [
                                        "head" => "Subscritores da lista:",
                                        "func" => 'listmembers'
                                                       ],
    "https://api.twitter.com/1.1/lists/subscriptions.json" => [
                                        "head" => "Listas subscritas por:",
                                        "func" => 'favoriteslist'
                                                       ],
    "https://api.twitter.com/1.1/statuses/user_timeline.json" => [
                                       "head" => "Tweets de:",
                                       "func" => 'favoriteslist'
                                                      ],
    "https://api.twitter.com/1.1/users/lookup.json" => [
                                       "head" => "Info de:",
                                       "func" => 'favoriteslist'
                                                      ],
    "https://api.twitter.com/1.1/users/search.json" => [
                                       "head" => "Termos de pesquisa nos utilizadores:",
                                       "func" => 'searchtweets'
                                                      ],
    "https://api.twitter.com/1.1/users/show.json" => [
                                       "head" => "Info de:",
                                       "func" => 'favoriteslist'
                                                      ],
                        ];
/**
 * @since 0.6
 * @var string VERSION Versão da aplicação
 */
 const VERSION = '1.1';

# ----- config end -----

/**
 * funções genéricas
 */
require_once('misc.php');



/**
 * Gera o html para a página de visualização de topo associada a vários ficheiros de
 * gathering (em $aconfig), e coloca na diretoria $outdir. Os ficheiros resultantes do
 * meaning.php para o gathering de $aconfig estão na diretoria $meandir.
 *
 * @since 0.5
 *
 * @param  array $outdir    Path para a diretoria onde coloca o html para a página de
 *                           visualização de topo associada a vários ficheiros de gathering (em $aconfig).
 * @param string $meandir Path para a diretoria onde estão os ficheiros resultantes do
 *                        meaning.php para o gathering de $aconfig.
 * @param  string $config Ficheiro de configuração para gathering, relacionado com os ficheiros
 *                        colocados em $meandir.
 * @param string $baseurl URL de base dos ficheiros na directoria $meandir.
 * @param string $csvpath Path para a directoria $meandir a ser incluida no html como csv_path.
 * @param string $section Secção com info geral em $config
 * @return bool         True, se gerou o html. False, caso contrário.
 */
function genSection($outdir, $meandir, $config, $baseurl, $csvpath, $section = GENERAL_SECTION)
{
    $htmlfile = $outdir."/".pathinfo($meandir)['filename'] . '.' . "html";
    $fpo = fopen($htmlfile, 'w');

    $ageneral = get_conf_section($section, $config);
    if ($ageneral == []) {
        fwrite(STDERR, "Warning: unable to get section $section from configuration file $config.\n");
        return false;
    }
    fwrite($fpo, "<h3>".$ageneral[HEADER]."</h3>\n".$ageneral[DESCRIPTION]."\n");
    $asects = get_sections_name($config, [$section]);
    if ($asects == []) {
        fwrite(STDERR, "Warning: unable to get sections from configuration file $config.\n");
        return false;
    }
    foreach ($asects as $sects) {
        $asearch = process_searchconfig($sects, [], $config);
        if (empty($asearch) || !array_key_exists(GENERAL_SECTION, $asearch) ||
                    !array_key_exists("url", $asearch[GENERAL_SECTION])) {
            fwrite(STDERR, "Warning: unable to get url from configuration file $config.\n");
            continue;
        }
        $url = $asearch[GENERAL_SECTION]["url"];
        if (!array_key_exists($url, $GLOBALS['VIS_DIRECTIVES'])) {
            fwrite(STDERR, "Warning: URL $url not defined in VIS_DIRECTIVES.\n");
            continue;
        }
        fwrite($fpo, "<p>\n<h4>[".$GLOBALS['VIS_DIRECTIVES'][$url]["head"]."]</h4>\n");
        $text = $GLOBALS['VIS_DIRECTIVES'][$url]["func"]($asearch, $baseurl, $sects);
        fwrite($fpo, "$text</p>");
        genhtml2csv($asearch, $csvpath, $sects, $meandir, $ageneral[HEADER]);
    }
    fclose($fpo);
}



/**
 * Função que gera os html que vão carregar os csv.
 *
 * @since 0.5
 *
 * @param  array $asearch array com a seccção de config, resultante de process_searchconfig().
 * @param string $csvpath Path para o csv (csv_path no html).
 * @param string $sect nome da secção de pesquisa da qual $asearch são os seus elementos.
 * @param string $meandir Path para a diretoria onde é escrita a página html
 * @param string $header Header a colocar no ht,l
 * @return bool   true, se gerou os html; false caso contrário.
 */
function genhtml2csv($asearch, $csvpath, $sect, $meandir, $header)
{
    if (empty($asearch)) {
        return false;
    }
    $rstr1 = <<<EOF
<base href="https://cyberintell.projj.eu/dashboard/CyberIntell">
<link href="/css/bootstrap.min.css" rel="stylesheet">
<link href="https://cdn.datatables.net/1.10.11/css/dataTables.bootstrap.min.css" rel="stylesheet">
<link href="https://cdn.datatables.net/fixedheader/3.1.1/css/fixedHeader.dataTables.min.css" rel="stylesheet">
<link href="https://cdn.datatables.net/colreorder/1.3.1/css/colReorder.dataTables.min.css" rel="stylesheet">
<link href="https://cdn.datatables.net/rowreorder/1.1.1/css/rowReorder.dataTables.min.css" rel="stylesheet">

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->

<div class="container-fluid">
EOF;
    $rstr2 = <<<EOF
<p>
<hr>
<p>
<strong>Nota:</strong> Todos os dados são carregados em memória, pelo que o tempo de demora para a sua apresentação
depende das características do seu computador. Como contrapartida, a pesquisa ("Search") é efectuada sobre todos os dados e é
imediata.
</p>
<hr>
</p>
<div id='table-container'></div>

</div><!-- /.container -->

<footer class='footer'>
<div class='container-fluid'>
<hr />
<p class='pull-right'>CyberIntell</p>
</div>
</footer>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="/js/bootstrap.min.js"></script>
<script src="/js/jquery.csv.min.js"></script>
<script src="https://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.11/js/dataTables.bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/fixedheader/3.1.1/js/dataTables.fixedHeader.min.js"></script>
<script src="https://cdn.datatables.net/colreorder/1.3.1/js/dataTables.colReorder.min.js"></script>
<script src="https://cdn.datatables.net/rowreorder/1.1.1/js/dataTables.rowReorder.min.js"></script>
<script src='/js/csv_to_html_table.js'></script>
<script>
init_table({
EOF;
    $rstr3 = <<<EOF
element: 'table-container',
allow_download: false,
csv_options: {separator: ';', delimiter: '"'},
datatables_options: {"paging": true, "ordering": true, "processing": true,  "pagingType": "full_numbers",
    "lengthMenu": [ [20, 50, 90, -1], [20, 50, 90, "All"] ], "pageLength": 20, "dom": 'lfrptip',
    "autoWidth": true, "order": [],
    fixedHeader: { header: true, footer: false },
    rowReorder: true,
    colReorder: true
    }
});
</script>
</body>
</html>
EOF;
    $rstrglob = $rstr1 . "\n<h2>CyberIntell - $header</h2>\n<p>\nTermo(s) de pesquisa - Todos -:\n";
    $nth = [400, 800, 2000];
    foreach (array_keys($asearch) as $search) {
        if ($search == GENERAL_SECTION) {
            continue;
        }
        $rstrglob .= "<ul>\n";
        $rstring = $rstr1 . "\n<h2>CyberIntell - $header</h2>\n<p>\nTermo(s) de pesquisa:<ul>\n";
        foreach ($asearch[$search] as $key => $value) {
            $rstring .= "<li>".$key." = ".$value."</li>\n";
            $rstrglob .= "<li>".$key." = ".$value."</li>\n";
        }
        $rstrglob .= "</ul>\n";
        $rstring .= "</ul>\n" . $rstr2;
        foreach ($nth as $n) { // ficheiros html de acordo com os csv's produzidos
            $fpo = fopen("$meandir/$sect.$search.$n.html", 'w');
            fwrite($fpo, $rstring . 'csv_path: "' . "$csvpath/$sect.$search.csv.$n" . '",' . "\n" . $rstr3);
            fclose($fpo);
        }
        $fpo = fopen("$meandir/$sect.$search.html", 'w');
        fwrite($fpo, $rstring . 'csv_path: "' . "$csvpath/$sect.$search.csv" . '",' . "\n" . $rstr3);
        fclose($fpo);
    }
    $rstrglob .= $rstr2;
    foreach ($nth as $n) { // ficheiros html de acordo com os csv's produzidos
        $fpo = fopen("$meandir/$sect.$n.html", 'w');
        fwrite($fpo, $rstrglob . 'csv_path: "' . "$csvpath/$sect.csv.$n" . '",' . "\n" . $rstr3);
        fclose($fpo);
    }
    $fpo = fopen("$meandir/$sect.html", 'w');
    fwrite($fpo, $rstrglob . 'csv_path: "' . "$csvpath/$sect.csv" . '",' . "\n" . $rstr3);
    fclose($fpo);

    return true;
}


/**
 * Função de escrita para página de visualização de topo, indicada em $GLOBALS['VIS_DIRECTIVES'].
 *
 * @since 0.6
 *
 * @param  array $asearch array com a seccção de config, resultante de process_searchconfig().
 * @param string $burl Base url.
 * @param string $sect nome da secção de pesquisa da qual $asearch são os seus elementos.
 * @return string   Informação a escrever na página de visualização de topo.
 */
function searchtweets($asearch, $burl, $sect)
{
    if (empty($asearch)) {
        return "";
    }
    $rstring = "<ul>\n";
    $rstring .= "  <li> <a href='$burl/$sect.400.html'><strong>Todos os termos de pesquisa</strong></a>" .
        " - Últimas [<a href='$burl/$sect.400.html'>400</a>, <a href='$burl/$sect.800.html'>800</a>, " .
        "<a href='$burl/$sect.2000.html'>2000</a>] entradas (<a href='$burl/$sect.html'>Todas as entradas</a>)" .
        "</li>\n";
    foreach (array_keys($asearch) as $search) {
        if ($search == GENERAL_SECTION) {
            continue;
        }
        $rstring .= "  <li> <a href='$burl/$sect.$search.400.html'><strong>".$asearch[$search]["q"]."</strong></a>" .
            " - Últimas [<a href='$burl/$sect.$search.400.html'>400</a>, <a href='$burl/$sect.$search.800.html'>800</a>, " .
            "<a href='$burl/$sect.$search.2000.html'>2000</a>] entradas (<a href='$burl/$sect.$search.html'>Todas as entradas</a>)" .
            "</li>\n";
    }
    $rstring .= "</ul>\n";
    return $rstring;
}


/**
 * Função de escrita para página de visualização de topo, indicada em $GLOBALS['VIS_DIRECTIVES'].
 *
 * @since 0.6
 *
 * @param  array $asearch array com a seccção de config, resultante de process_searchconfig().
 * @param string $burl Base url.
 * @param string $sect nome da secção de pesquisa da qual $asearch são os seus elementos.
 * @return string   Informação a escrever na página de visualização de topo.
 */
function favoriteslist($asearch, $burl, $sect)
{
    if (empty($asearch)) {
        return "";
    }
    $rstring = "<ul>\n";
    $rstring .= "  <li> <a href='$burl/$sect.400.html'><strong>Todos os termos de pesquisa</strong></a>" .
        " - Últimas [<a href='$burl/$sect.400.html'>400</a>, <a href='$burl/$sect.800.html'>800</a>, " .
        "<a href='$burl/$sect.2000.html'>2000</a>] entradas (<a href='$burl/$sect.html'>Todas as entradas</a>)" .
        "</li>\n";
    foreach (array_keys($asearch) as $search) {
        if ($search == GENERAL_SECTION) {
            continue;
        }
        $screenname = $asearch[$search]["screen_name"];
        $rstring .= "  <li> <a href='$burl/$sect.$search.400.html'><strong>@$screenname</strong></a>" .
            " - Últimas [<a href='$burl/$sect.$search.400.html'>400</a>, <a href='$burl/$sect.$search.800.html'>800</a>, " .
            "<a href='$burl/$sect.$search.2000.html'>2000</a>] entradas (<a href='$burl/$sect.$search.html'>Todas as entradas</a>)" .
            "</li>\n";
    }
    $rstring .= "</ul>\n";
    return $rstring;
}

/**
 * Função de escrita para página de visualização de topo, indicada em $GLOBALS['VIS_DIRECTIVES'].
 *
 * @since 0.6
 *
 * @param  array $asearch array com a seccção de config, resultante de process_searchconfig().
 * @param string $burl Base url.
 * @param string $sect nome da secção de pesquisa da qual $asearch são os seus elementos.
 * @return string   Informação a escrever na página de visualização de topo.
 */
function listmembers($asearch, $burl, $sect)
{
    if (empty($asearch)) {
        return "";
    }
    $rstring = "<ul>\n";
    $rstring .= "  <li> <a href='$burl/$sect.400.html'><strong>Todos os termos de pesquisa</strong></a>" .
        " - Últimas [<a href='$burl/$sect.400.html'>400</a>, <a href='$burl/$sect.800.html'>800</a>, " .
        "<a href='$burl/$sect.2000.html'>2000</a>] entradas (<a href='$burl/$sect.html'>Todas as entradas</a>)" .
        "</li>\n";
    foreach (array_keys($asearch) as $search) {
        if ($search == GENERAL_SECTION) {
            continue;
        }
        $listid = (array_key_exists("slug", $asearch[$search])) ? $asearch[$search]["slug"] :
                                                                    $asearch[$search]["list_id"] ;
        $rstring .= "  <li> <a href='$burl/$sect.$search.400.html'><strong>$listid</strong></a>" .
            " - Últimas [<a href='$burl/$sect.$search.400.html'>400</a>, <a href='$burl/$sect.$search.800.html'>800</a>, " .
            "<a href='$burl/$sect.$search.2000.html'>2000</a>] entradas (<a href='$burl/$sect.$search.html'>Todas as entradas</a>)" .
            "</li>\n";
    }
    $rstring .= "</ul>\n";
    return $rstring;
}



/**
 * Função de execução do programa
 * @param  string $options  Similar ao primeiro argumento de getopt().
 * @param  array $longopts  Similar ao segundo argumento de getopt().
 * @param  int $nparams     Número de argumentos obrigatórios da linha de comando + 1.
 * @param  array $arg   Argumentos da linha de comando (deverá ser $argv).
 * @return int
 */
function main($options, $longopts, $nparams, $arg)
{
    $aopt = getopt($options, $longopts);
    if (empty($aopt) && count($arg) > $nparams) {
        fwrite(STDOUT, "Usage: php $arg[0] [--h] [--v] -p <directory path to top visualization html> -m <directory path to meaning output directory> -b <base URL to meaning output directory> -c <csv_path directory to meaning output directory> -f <gathering config filename>\n");
        return 0;
    } elseif (array_key_exists("h", $aopt)) {
        fwrite(STDOUT, "Usage: php $arg[0] [--h] [--v] -p <directory path to top visualization html> -m <directory path to meaning output directory> -b <base URL to meaning output directory> -c <csv_path directory to meaning output directory> -f <gathering config filename>\n");
        fwrite(STDOUT, "   --h \t\thelp\n");
        fwrite(STDOUT, "   --v \t\tversion\n");
        fwrite(STDOUT, "   -p \t\tpath to the directory where the html for the top visualization page will be written\n");
        fwrite(STDOUT, "   -m \t\tpath to the output directory of meaning.php\n");
        fwrite(STDOUT, "   -b \t\tbase URL to the output directory of meaning.php\n");
        fwrite(STDOUT, "   -c \t\tpath to the output directory of meaning.php to be included in the html file as the csv_path\n");
        fwrite(STDOUT, "   -f \t\tgathering configuration file, related to the data gathered, processed and meaning'd to -m\n");
        fwrite(STDOUT, "\n$arg[0] - Generates all the necessary html to visualize the data gathered (for GUI based on csv-to-html-table and huge).\n\n");
        return 0;
    } elseif (array_key_exists("v", $aopt)) {
        fwrite(STDOUT, "$arg[0] \nVersion:" . VERSION. "\n");
        return 0;
    }
    genSection($aopt["p"], $aopt["m"], $aopt["f"], $aopt["b"], $aopt["c"]);
    return 0;
}

exit(main("p:m:b:c:f:", ["h", "v"], 1, $argv));

//php consumption.php -p /home/jepm/PG_CSCD/CyberIntell/data/meaning -m /home/jepm/PG_CSCD/CyberIntell/data/meaning/bizprocess1 -b https://cyberintell.projj.eu/dashboard/CyberIntell/bizprocess1 -f ../../config/twitter.bizgather.process1.conf -c CyberIntell/bizprocess1
