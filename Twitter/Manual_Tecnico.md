# CyberIntell Twitter - Manual técnico

## 1. CyberIntell Twitter

O CyberIntell Twitter é uma implementação de uma feed e aplicação capaz de recolher dados e monitorizar/alertar para a ocorrência de dados/ameaças
no [twitter](https://twitter.com/).

Está organizada em pequenas aplicações de acordo com o *Threat Intelligence Life Cycle*:
+ Gather - aplicação `gather.php` que vai buscar os dados de acordo com as pesquisas de dados/ameaças a efectuar;
+ Process - aplicação `process.php` que processa e normaliza os dados obtidos na fase anterior;
+ Create Meaning - aplicação `meaning.php` que pega nos dados processados e os filtra e agrupa/separa, de modo a serem mais facilmente analisados pelos analistas;
+ Consumption - aplicação `consumption.php` que gera informação adicional, de modo a que os dados obtidos na fase anterior possam ser apresentados aos analistas num ambiente ou aplicação de visualização.

O objetivo é poder ser instalada numa plataforma extremamente low-cost (máquina 4-core ARMv7 CPU com 2GB de RAM
e 50 GB SSD) que possa ser utilizada pelas PME e micro-empresas, assim como em plataforma baseada numa [stack ELK][elk].


Pode obter a versão mais actual através do download da última versão da branch
[master](https://gitlab.com/devisefutures/CyberIntell/repository/archive.tar.gz?ref=master). A instalação e configuração estão
descritas no [Manual do Utilizador](https://gitlab.com/devisefutures/CyberIntell/blob/master/Twitter/README.md).


## 2. Configuração das aplicações

Cada aplicação do CyberIntell Twitter (à excepção do `consumption.php`) tem um ficheiro de configuração, com o objetivo de:
+ garantir a maior flexibilidade na obtenção, tratamento e visualização dos dados,
+ permitir organizar as pesquisas por processos autónomos, não misturando temas - i.e., permitir que as várias pesquisas sobre um tema estejam no mesmo ficheiro de configuração, sem estarem misturadas com outras pesquisas sobre outros temas -, e
+ possibilitar que os ficheiros de output de uma fase possam ser processados de diferentes maneiras nas fases seguintes.


## 2.1 gather.php

Na aplicação `gather.php`  podem ser configuradas as seguintes pesquisas, de acordo com os parâmetros de pesquisa na
[documentação de referência das APIs do Twitter](https://dev.twitter.com/rest/reference):
* [GET favorites/list](https://dev.twitter.com/rest/reference/get/favorites/list)
* [GET followers/ids](https://dev.twitter.com/rest/reference/get/followers/ids)
* [GET followers/list](https://dev.twitter.com/rest/reference/get/followers/list)
* [GET friends/ids](https://dev.twitter.com/rest/reference/get/friends/ids)
* [GET friends/list](https://dev.twitter.com/rest/reference/get/friends/list)
* [GET geo/search](https://dev.twitter.com/rest/reference/get/geo/search)
* [GET lists/list](https://dev.twitter.com/rest/reference/get/lists/list)
* [GET lists/members](https://dev.twitter.com/rest/reference/get/lists/members)
* [GET lists/memberships](https://dev.twitter.com/rest/reference/get/lists/memberships)
* [GET lists/ownerships](https://dev.twitter.com/rest/reference/get/lists/ownerships)
* [GET lists/show](https://dev.twitter.com/rest/reference/get/lists/show)
* [GET lists/statuses](https://dev.twitter.com/rest/reference/get/lists/statuses)
* [GET lists/subscribers](https://dev.twitter.com/rest/reference/get/lists/subscribers)
* [GET lists/subscriptions](https://dev.twitter.com/rest/reference/get/lists/subscriptions)
* [GET search/tweets](https://dev.twitter.com/rest/reference/get/search/tweets)
* [GET statuses/user_timeline](https://dev.twitter.com/rest/reference/get/statuses/user_timeline)
* [GET trends/available](https://dev.twitter.com/rest/reference/get/trends/available)
* [GET users/lookup](https://dev.twitter.com/rest/reference/get/users/lookup)
* [GET users/search](https://dev.twitter.com/rest/reference/get/users/search)
* [GET users/show](https://dev.twitter.com/rest/reference/get/users/show)
* [GET users/suggestions](https://dev.twitter.com/rest/reference/get/users/suggestions)

As restantes pesquisas GET não parecem ser tão importantes como estas, pelo que não podem ser configuradas no ficheiro de configuração
da aplicação `gather.php`. Contudo, em próximas versões do CyberIntell Twitter poderão ser implementadas mais pesquisas GET se exisitr essa
necessidade.

### 2.1.1 Ficheiro de Configuração

O ficheiro de configuração tem o formato de um ficheiro .ini do php. Resumidamente, contém:
+ início de secção, identificado por uma linha com o seguinte conteúdo `[nome_da_secção]` - a secção finaliza no início da próxima secção (ou no final do ficheiro, caso não existam mais secções),
+ atribuição do tipo `variavel = valor` ou `indice[array] = valor`,
+ comentários - começam por `;`.

O ficheiro de configuração do `gather.php` começa por uma secção de configuração geral [general] onde são definidas as seguintes variáveis:
+ `output` - diretoria de output (terminada em /),
+ `proc` - diretoria de informação utilizada para optimizar as pesquisas no twitter (terminada em /),
+ `header` - Título a dar a esta pesquisa (a utilizar na fase de consumption),
+ `description` - Descrição da pesquisa (a utilizar na fase de consumption).

De seguida, o ficheiro de configuração tem secções de pesquisa, uma por cada API diferente que for utilizada na pesquisa. Dentro de cada secção de pesquisa
é indicado o url da API (`url[general]`) e, opcionalmente, o parâmetro obrigatório dessa pesquisa (`required[general]`).

Cada secção de pesquisa pode ter várias pesquisas desse tipo, sendo utilizada a notação
`nome_parametro_pesquisa_conforme_API[ID_pesquisa] = 'valor_do_parametro_de_pesquisa'`

Exemplo:
```
[twitter-st]
url[general] = 'https://api.twitter.com/1.1/search/tweets.json'
required[general] = 'q'
;
; ID pesquisa ciber
q[ciber] = 'ciberseguranca OR ciberdefesa'
lang[ciber] = 'pt'
result_type[ciber] = 'mixed'
count[ciber] = '100'
; ID pesquisa OSINT
q[osint] = 'OSINT'
```

Note-se que se a API não tiver parâmetros, como é o caso da GET trends/available, o nome_parametro_pesquisa_conforme_API será none
(e.g., `none[tra] = ''`).

O ficheiro de configuração por omissão - twitter.gather.conf (na directoria Twitter do CyberIntell) - tem exemplos para todas as APIs que podem
ser pesquisadas.

### 2.1.2 Optimização da pesquisa

O `gather.php` tira partido dos mecanismos de optimização de pesquisas nas respostas do Twitter ('next_cursor' e 'refresh_url'), sempre que estes existem.
Deste modo, em cada chamada ao `gather.php` vão-se buscar as próximas respostas, não havendo repetição de respostas.

### 2.1.3 Ficheiros de pesquisa

Todas as respostas são guardadas na diretoria de output configurada no ficheiro de configuração, em formato json, sem qualquer processamento. O nome dos
ficheiros segue a seguinte convenção: `nome_da_secção_de_pesquisa.ID_pesquisa.timestamp.json`.

## 2.2 process.php

Na aplicação `process.php` é configurado o tipo de normalização de dados pretendida para o resultado das pesquisas da fase de Gather.

Existem duas fases de processamento:
+ normalização, em que os dados recolhidos na fase anterior são normalizados - este processamento é obrigatório para cada pesquisa efectuada na fase anterior;
+ pós-processamento, em que é verificado qual a informação obtida na fase de recolha que deve ser guardada - este processamento é opcional para cada pesquisa efectuada na fase anterior.

### 2.2.1 Ficheiro de Configuração

#### 2.2.1.1 Ficheiro de Configuração para o processamento de normalização

O ficheiro de configuração tem o formato de um ficheiro .ini do php. Resumidamente, contém:
+ início de secção, identificado por uma linha com o seguinte conteúdo `[nome_da_secção]` - a secção finaliza no início da próxima secção (ou no final do ficheiro, caso não existam mais secções),
+ atribuição do tipo `variavel = valor` ou `indice[array] = valor`,
+ comentários - começam por `;`.

O ficheiro de configuração de normalização do `process.php` começa por uma secção de configuração geral [general] onde são definidas as seguintes variáveis:
+ `input` - diretoria de input (terminada em /), que corresponde à diretoria de output da fase de gather, que se pretende normalizar de acordo com este ficheiro,
+ `output` - diretoria de output (terminada em /),

De seguida, o ficheiro de configuração tem secções de normalização - até uma secção de normalização por cada secção de pesquisa (e com o mesmo nome da secção
de pesquisa) na fase de gather -. Cada linha de normalização dentro da secção de normalização utiliza a notação `n[nome_coluna_output] = 'nome_campo_json'`, sendo
que no 'nome_campo_json' é utilizado '/' para indicar a navegação em sub-campos.

A mesma normalização é aplicada aos vários IDs de pesquisa definidos, na fase de gather, numa secção de pesquisa com o nome igual ao da secção de normalização.


Exemplo:
```
[twitter-st]
n['created_at'] = 'created_at'
n['noticia_original'] = 'entities/urls/0/url'
n['favorite_count'] = 'favorite_count'
n['twitter_id'] = 'id_str'
n['retweet_count'] = 'retweet_count'
n['source'] = 'source'
n['tweet_text'] = 'text'
n['user_created_at'] = 'user/created_at'
n['user_description'] = 'user/description'
n['user_favorites'] = 'user/favourites_count'
n['user_followers'] = 'user/followers_count'
n['user_friends'] = 'user/friends_count'
n['user_id'] = 'user/id_str'
n['user_location'] = 'user/location'
n['username'] = 'user/name'
n['user_image'] = 'user/profile_image_url_https'
n['user_screenname'] = 'user/screen_name'
n['user_tweets'] = 'user/statuses_count'
```

O ficheiro de configuração por omissão - twitter.process.conf (na directoria Twitter do CyberIntell) - tem exemplos para todas as secções de pesquisa da
fase de gather.

#### 2.2.1.1 Ficheiro de Configuração para o pós-processamento

Tornou-se óbvia a necessidade de existir pós-processamento dos dados recebidos, quando se verificou que a API de pesquisa de tweet
('https://api.twitter.com/1.1/search/tweets.json') devolve muitos falsos positivos, já que:
+ a pesquisa não incide apenas no tweet, mas também nos dados associados ao tweet (autor, ...), e
+ a pesquisa não é por palavra, mas por verificação se a expressão de procura aparece no tweet e dados associados, o que muitas vezes não é o pretendido. Ou seja, se a
expressão de pesquisa for "are", são devolvidos todos os tweets que tiverem "are" no texto do tweet e dados associados - "care", "dare" "flareability", ... .

O ficheiro de configuração tem o formato de um ficheiro .ini do php. Resumidamente, contém:
+ início de secção, identificado por uma linha com o seguinte conteúdo `[nome_da_secção]` - a secção finaliza no início da próxima secção (ou no final do ficheiro, caso não existam mais secções),
+ atribuição do tipo `variavel = valor` ou `indice[array] = valor`,
+ comentários - começam por `;`.

O ficheiro de configuração de pós-processamento do `process.php` começa por uma secção de configuração geral [general] onde são definidas as seguintes variáveis:
+ `input` - diretoria de input (terminada em /), que corresponde à diretoria de output da fase de gather, que se pretende pós-processar de acordo com este ficheiro,

De seguida, o ficheiro de configuração tem secções de pós-processamento - até uma secção de pós-processamento por cada secção de pesquisa (e com o mesmo nome da secção
de pesquisa) e ID_pesquisa da fase de gather, sendo que o nome da secção é a concatenação do nome da secção de pesquisa com o ID de pesquisa (separado por '.') -.

Em cada secção de pós-processamento são definidos as várias validações, utilizando a seguinte notação:
+ nome_coluna['header'] = coluna_a_ser_validada (nome da coluna normalizada de acordo com o processamento de normalização)
+ nome_coluna['func'] = função_de_validação
+ nome_coluna['args'] = argumentos_da_função_de_validação (separadas por ';')



Exemplo:
```
[twitter-st.evotum]
tt['header'] = 'tweet_text'
tt['func'] = 'pptextIncludeAllWords'
tt['args'] = "evotum"
;
[twitter-st.osint]
xx['header'] = 'tweet_text'
xx['func'] = 'pptextIncludeAllWords'
xx['args'] = "osint"
```

As funções de pós-processamento disponíveis (no ficheiro pp_functions.php) são:
+ pptextIncludeAllWords
+ pptextIncludeOneWord
+ remDupCol
+ remDupColnoURL
+ remDupColnoRetweet

O pós-processamento é opcional. O ficheiro de configuração twitter.postprocess.conf (na directoria Twitter do CyberIntell) tem alguns exemplos.


### 2.2.2 Ficheiros de normalização

Os ficheiros normalizados são guardadas na diretoria de output configurada no ficheiro de configuração, em formato csv. O nome dos
ficheiros segue a seguinte convenção: `nome_da_secção_de_normalizacao.ID_pesquisa.timestamp.json`.


### 2.2.3 Optimização da normalização

Em cada chamada do `process.php` só são normalizados os ficheiros na diretoria de input cujo timestamp é mais recente do que o ficheiro normalizado na
diretoria de output. Nesse caso, a normalização é adicionada ao ficheiro na diretoria de output, que actualiza o seu timestamp.


## 2.3 meaning.php

Na aplicação `meaning.php` é configurado o tipo de agrupamento que os dados devem ter quando são analisados pelos analistas, sendo também formatados com o
aspecto com que devem ser visualizados.

### 2.3.1 Ficheiro de Configuração

O ficheiro de configuração tem o formato de um ficheiro .ini do php. Resumidamente, contém:
+ início de secção, identificado por uma linha com o seguinte conteúdo `[nome_da_secção]` - a secção finaliza no início da próxima secção (ou no final do ficheiro, caso não existam mais secções),
+ atribuição do tipo `variavel = valor` ou `indice[array] = valor`,
+ comentários - começam por `;`.

O ficheiro de configuração do `meaning.php` começa por uma secção de configuração geral [general] onde são definidas as seguintes variáveis:
+ `input` - diretoria de input (terminada em /), que corresponde à diretoria de output da fase de process,
+ `output` - diretoria de output (terminada em /),

De seguida, o ficheiro de configuração tem secções de agrupamento e formatação - até uma secção de normalização por cada secção de pesquisa (e com o mesmo nome da secção
de pesquisa) na fase de gather -.

Em cada secção de agrupamento e formatação são definidos as várias colunas do output, indicando para cada coluna, o cabeçalho, o agrupamento da(s) coluna(s) de input e a função de formatação, utilizando a seguinte notação:
+ nome_coluna['header'] = cabeçalho
+ nome_coluna['func'] = função_de_formatação
+ nome_coluna['args'] = colunas_de_input separadas por ';'

O mesmo agrupamento e formatação é aplicada aos vários IDs de pesquisa definidos, na fase de gather, numa secção de pesquisa com o nome igual ao da secção
de agrupamento.


Exemplo:
```
[twitter-st]
created_at['header'] = 'Criado em'
created_at['func'] = 'created'
created_at['args'] = "created_at"
;
username['header'] = 'Utilizador'
username['func'] = 'fulluserwithphoto'
username['args'] = "username;user_location;user_screenname;user_description;user_followers;user_friends;user_favorites;user_tweets;user_id;user_created_at;user_image"
;
tweet_text['header'] = 'Texto do Tweet'
tweet_text['func'] = 'text'
tweet_text['args'] = "tweet_text;user_screenname;twitter_id;retweet_count;noticia_original;source;favorite_count"
```

As funções de formatação disponíveis (no ficheiro format_functions.php) são:
+ created
+ text
+ listdescription
+ fulluserwithphoto
+ fulluser
+ foto


O ficheiro de configuração por omissão - twitter.process.conf (na directoria Twitter do CyberIntell) - tem exemplos para todas as secções de pesquisa da
fase de gather.


### 2.3.2 Ficheiros formatados

Os ficheiros formatados são guardadas na diretoria de output configurada no ficheiro de configuração, em formato csv. O nome dos
ficheiros segue a seguinte convenção: `nome_da_secção_de_formatacao.ID_pesquisa.json`.


### 2.3.3 Optimização da formatação

Em cada chamada do `meaning.php` todos os ficheiros na directoria de input são processados, reescrevendo (ou criando novos, caso ainda não existam) is ficheiros
anteriores na diretoria de output. Optou-se por não se optimizar esta fase, de modo a garantir que o output é sempre gerado com o agrupamento e formatação mais
recente.


## 2.4 consumption.php

A aplicação `consumption.php`  gera informação adicional, de modo a que os dados obtidos na fase anterior possam ser apresentados aos analistas num ambiente ou
aplicação de visualização.

A implementação actual foi feita especificamente para que os dados possam ser apresentados através do [csv-to-html-table](https://github.com/derekeder/csv-to-html-table)
e [huge](https://github.com/panique/huge).

Esta aplicação não possui um ficheiro de configuração, mas o array VIS_DIRECTIVES (no ficheiro consumption.php) pode ser alterada, caso se pretenda
alterar o html gerado para a página de visualização de topo. Este array indica o cabeçalho e a função a gerar o html de visualização de topo para
cada API de pesquisa.

Exemplo:
```
"https://api.twitter.com/1.1/search/tweets.json" => [
                                    "head" => "Termos de pesquisa nos tweets:",
                                    "func" => 'searchtweets'
                                                    ],
```

As funções disponíveis são:
+ searchtweets
+ favoriteslist
+ listmembers

O ficheiro consumption.php tem as definições apropriadas para as várias APIs de pesquisa.


## 3. crontab

As aplicações desenvolvidas podem ser chamadas a partir do **crontab**, sequencialmente (na ordem do *Threat Intelligence Life Cycle*), acumulando
o resultados das várias pesquisas efectuadas. Os ficheiros de configuração podem ser alterados, reflectindo-se essas alterações na próxima chamada da
respectiva aplicação.

Recomenda-se que o intervalo em que são chamadas as aplicações não seja inferior a 15 minutos (por cada ficheiro adicional de pesquisa que chame a mesma API,
aumentar este valor em mais 15 minutos), de modo a não ativar as restrições de pesquisa do Twitter.


## 4. Exemplos

Ver secção de exemplos no [Manual do Utilizador](https://gitlab.com/devisefutures/CyberIntell/blob/master/Twitter/README.md).
