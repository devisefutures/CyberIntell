# CyberIntell Twitter - Manual do utilizador

## 1. CyberIntell Twitter

O CyberIntell Twitter é uma implementação de uma feed e aplicação capaz de recolher dados e monitorizar/alertar para a ocorrência de dados/ameaças
no [twitter](https://twitter.com/).

Está organizada em pequenas aplicações de acordo com o *Threat Intelligence Life Cycle*:
+ Gather - aplicação `gather.php` que vai buscar os dados de acordo com as pesquisas de dados/ameaças a efectuar;
+ Process - aplicação `process.php` que processa e normaliza os dados obtidos na fase anterior;
+ Create Meaning - aplicação `meaning.php` que pega nos dados processados e os filtra e agrupa/separa, de modo a serem mais facilmente analisados pelos analistas;
+ Consumption - aplicação `consumption.php` que gera informação adicional, de modo a que os dados obtidos na fase anterior possam ser apresentados aos analistas num ambiente ou aplicação de visualização.

O objetivo é poder ser instalada numa plataforma extremamente low-cost (máquina 4-core ARMv7 CPU com 2GB de RAM
e 50 GB SSD) que possa ser utilizada pelas PME e micro-empresas, assim como em plataforma baseada numa [stack ELK][elk].


## 2. Instalação

Copie a última versão da branch [master](https://gitlab.com/devisefutures/CyberIntell/repository/archive.tar.gz?ref=master)
da CyberIntell para o seu computador. De seguida, aceda à diretoria Twitter e efectue o seguinte comando:
```
composer install
```
para instalar a twitter-api-php](https://github.com/J7mbo/twitter-api-php).

### 2.1 Dependências

O código foi testado em _ambiente Linux e MacOS_, com as seguintes versões de software:
+ **composer**, versão 1.1.0 ou superior,
+ **php**, versão 5.6.30 ou superior,
+ **bash**, versão 3.2.57 ou superior.

A CyberIntell Twitter depende ainda da [twitter-api-php](https://github.com/J7mbo/twitter-api-php) - commit 5a1ac487adc248b59a73d5edc002b57f8dc211b8 -.

## 3. Configuração

Para utilizar esta aplicação tem que efectuar os seguintes passos:
1. Crie uma app twitter no site [Twitter Apps](https://apps.twitter.com/)
  + Indique o nome e a descrição que desejar;
  + No website, pode introduzir o URL do seu website (ou http://localhost.localdomain);
  + Selecione "*Yes, I have read and agree to the Twitter Developer Agreement.*" e crie a app;
  + Na tab "*Keys and Access Tokens*" selecione "*Create my access token*";
  + Guarde os valores associados *Consumer Key (API Key)*, *Consumer Secret (API Secret)*, *Access Token* e *Access Token Secret*.
2. Aceda à directoria Twitter da CyberIntell e renomeie o ficheiro `twitter.__conf` para `twitter.conf`.
3. Abra o ficheiro `twitter.conf` e substitua:
  + OAUTH pelo valor associado a *Access Token*;
  + SECRET pelo valor associado a *Access Token Secret*;
  + KEY pelo valor associado a *Consumer Key (API Key)*;
  + CONSUMER_SECRET pelo valor associado a *Consumer Secret (API Secret)*.

## 4. Utilização

O utilizador destas aplicações pode ser directamente o analista de ameaças, ou então, o analista de ameaças define o que quer estar a monitorizar
no twitter e um técnico configura as aplicações de modo a que o analista possa aceder à apresentação visual das respostas.

## 4.1 gather.php

A aplicação `gather.php`  vai buscar os dados de acordo com as pesquisas de dados/ameaças a efectuar, configuradas no ficheiro de configuração - por
omissão é utilizado o ficheiro de configuração twitter.gather.conf (na directoria Twitter do CyberIntell) -. O
[manual técnico](https://gitlab.com/devisefutures/CyberIntell/blob/master/Twitter/Manual_Tecnico.md) inclui informação detalhada sobre o modo de criar
o ficheiro de configuração para a app `gather.php`.

A aplicação `gather.php` é utilizada do seguinte modo:
```
Utilização: php gather.php [--h] [-f <ficheiro config>]
   --h 		ajuda
   --v 		versão
   -f 		ficheiro de configuração com as pesquisas a efetuar
```

## 4.2 process.php

A aplicação `process.php`  processa e normaliza os dados obtidos na fase anterior, de acordo com a configuração nos ficheiros de configuração - por
omissão é utilizado o ficheiro de configuração twitter.process.conf (na directoria Twitter do CyberIntell) para o processamento -. O
[manual técnico](https://gitlab.com/devisefutures/CyberIntell/blob/master/Twitter/Manual_Tecnico.md) inclui informação detalhada sobre o modo de criar
o ficheiro de configuração para a app `process.php`.

A aplicação `process.php` é utilizada do seguinte modo:
```
Utilização: php process.php [--h] [--v] [-f <ficheiro config>]
   --h 		ajuda
   --v 		versão
   -f 		ficheiro de configuração para o processamento
   -p 		ficheiro de configuração para o pós-processamento   
```

## 4.3 meaning.php

A aplicação `neaning.php`  pega nos dados processados e filtra-os e agrupa/separa, de modo a serem mais facilmente analisados pelos analistas,
de acordo com a configuração no ficheiro de configuração - por omissão é utilizado o ficheiro de configuração twitter.meaning.conf (na
directoria Twitter do CyberIntell) -.
O [manual técnico](https://gitlab.com/devisefutures/CyberIntell/blob/master/Twitter/Manual_Tecnico.md) inclui informação detalhada sobre o modo de criar
o ficheiro de configuração para a app `meaning.php`.

A aplicação `meaning.php` é utilizada do seguinte modo:
```
Utilização: php meaning.php [--h] [--v] [-f <ficheiro config>]
   --h 		ajuda
   --v 		versão
   -f 		ficheiro de configuração para o meaning
```

## 4.4 consumption.php

A aplicação `consumption.php`  gera informação adicional, de modo a que os dados obtidos na fase anterior possam ser apresentados aos analistas num ambiente ou
aplicação de visualização.

A implementação actual foi feita especificamente para que os dados possam ser apresentados através do [csv-to-html-table](https://github.com/derekeder/csv-to-html-table)
e [huge](https://github.com/panique/huge).

O [manual técnico](https://gitlab.com/devisefutures/CyberIntell/blob/master/Twitter/Manual_Tecnico.md) inclui informação detalhada sobre configurações
adicionais na app `consumption.php`.


A aplicação `consumption.php` é utilizada do seguinte modo:
```
Utilização: php consumption.php [--h] [--v] -p <path para a diretoria de visualização de topo html> -m <path para a diretoria de output de meaning> -b <base URL para a diretoria de output de meaning> -c <path csv_path directory para a diretoria de output de meaning> -f <ficheiro de configuração de gather>
   --h 		ajuda
   --v 		versão
   -p 		path para a diretoria onde vai ser escrito o html para a página de visualização de topo
   -m 		path para a diretoria de output de meaning.php
   -b 		base URL para a diretoria de output de meaning.php
   -c 		path para a diretoria de output de meaning.php a ser incluida nos ficheiros html, no csv_path
   -f 		ficheiro de configuração de gather, relacionado com os dados obtidos, processados e filtrados para a diretoria indicada -m
   ```

## 5. Visualização

O analista poderá visualizar os dados monitorizados através de qualquer ambiente ou aplicação de visualização, sendo necessário desenvolver a
app `consumption.php` à medida desse ambiente.

Neste caso fornece-se já a app `consumption.php` desenvolvida à medida para que a visualização dos dados seja efectuada através do
[csv-to-html-table](https://github.com/derekeder/csv-to-html-table) e [huge](https://github.com/panique/huge). Para a instalação e configuração
dessas duas aplicações, deverá consultar os respectivos manuais.

Em alternativa pode contactar um dos elementos que desenvolveu o CyberIntell para o pré-registar na [plataforma CyberIntell Twitter](https://cyberintell.projj.eu/) -
baseada em [csv-to-html-table](https://github.com/derekeder/csv-to-html-table) e [huge](https://github.com/panique/huge) - de forma a poder verificar o que
é possível fazer com o CyberIntell Twitter. Note que a [plataforma CyberIntell Twitter](https://cyberintell.projj.eu/) está instalado numa máquina extremamente
low-cost (máquina 4-core ARMv7 CPU com 2GB de RAM e 50 GB SSD).

## 6. Exemplos

Existem dois exemplos inicias que irão sendo melhorados à medida que a análise das respostas permita que o analista refine e/ou alargue o tipo de pesquisas.

Estes exemplos estão disponíveis na [plataforma CyberIntell Twitter](https://cyberintell.projj.eu/), estando as pesquisas a ser feitas de forma contínua.

## 6.1 Processo 1

Este processo foi inicializado por uma empresa na área da criptografia que pretende saber o que se passa nessa área, assim como com a concorrência. Adicionalmente vai seguir algumas pessoas para tentar descobrir novos cérebros que possa contratar.

As pesquisas iniciais configuradas pelo analista são as seguintes (secções de pesquisa do ficheiro de configuração da fase de Gather):
```
[searchtweets]
url[general] = 'https://api.twitter.com/1.1/search/tweets.json'
required[general] = 'q'
;
q[cripto] = 'cryptography OR encryption OR decryption OR #crypto OR steganography'
result_type [cripto] = 'recent'
count [cripto] = '100'
;
q[osint1] = 'OSINT crypto'
count [osint1] = '100'
result_type [osint1] = 'mixed'
;
q[osint2] = 'OSINT steganography'
count [osint2] = '100'
result_type [osint2] = 'mixed'
;
;
[listfavs]
url[general] = 'https://api.twitter.com/1.1/favorites/list.json'
;
screen_name[schneierblog] = 'schneierblog'
include_entities[schneierblog] = 'false'
;
screen_name[matthew_d_green] = 'matthew_d_green'
include_entities[matthew_d_green] = 'false'
;
screen_name[kevinmitnick] = 'kevinmitnick'
include_entities[kevinmitnick] = 'false'
;
;
[followerslist]
url[general] = 'https://api.twitter.com/1.1/followers/list.json'
;
screen_name[schneierblog] = 'schneierblog'
count[schneierblog] = '20'
;
screen_name[matthew_d_green] = 'matthew_d_green'
count[matthew_d_green] = '20'
;
screen_name[kevinmitnick] = 'kevinmitnick'
;
;
[friendslist]
url[general] = 'https://api.twitter.com/1.1/friends/list.json'
;
screen_name[schneierblog] = 'schneierblog'
count[schneierblog] = '200'
;
screen_name[matthew_d_green] = 'matthew_d_green'
count[matthew_d_green] = '100'
;
; ID pesquisa kevinmitnick
screen_name[kevinmitnick] = 'kevinmitnick'
;
;
[status_u_timeline]
url[general] = 'https://api.twitter.com/1.1/statuses/user_timeline.json'
;
screen_name[dumpmon] = 'dumpmon'
count[dumpmon] = '200'
;
screen_name[schneierblog] = 'schneierblog'
count[schneierblog] = '200'
;
screen_name[matthew_d_green] = 'matthew_d_green'
count[matthew_d_green] = '100'
;
screen_name[kevinmitnick] = 'kevinmitnick'
```


## 6.2 Processo 2

Este processo foi inicializado por uma empresa na área financeira, com investimento em ações norte-americanas que pretende saber o que se passa nesse mercado, assim como as notícias referentes às ações que possui em carteira. Adicionalmente vai seguir algumas pessoas para tentar descobrir novos cérebros que possa contratar.

As pesquisas iniciais configuradas pelo analista são as seguintes (secções de pesquisa do ficheiro de configuração da fase de Gather):
```
[searchtweets]
url[general] = 'https://api.twitter.com/1.1/search/tweets.json'
required[general] = 'q'
;
q[stockexchange] = 'stock exchange'
result_type [stockexchange] = 'mixed'
count [stockexchange] = '100'
lang[stockexchange] = 'en'
;
q[nasdaq] = 'nasdaq OR nyse'
count [nasdaq] = '100'
result_type [nasdaq] = 'mixed'
;
q[acoes] = '#AMD OR Exelixis OR Ionis OR #TSLA'
count [acoes] = '100'
result_type [acoes] = 'mixed'
;
;
[listfavs]
url[general] = 'https://api.twitter.com/1.1/favorites/list.json'
;
screen_name[markets] = 'markets'
include_entities[markets] = 'false'
```
