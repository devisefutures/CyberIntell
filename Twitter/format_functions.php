<?php
/**
* CyberIntell - feed e aplicação capaz de recolher dados e monitorizar/alertar
*              a ocorrência de dados/ameaças.
*
* format_functions.php
*     Funções de formatação disponíveis para serem indicadas no ficheiro de configuração de meaning.
*
* @link   https://gitlab.com/devisefutures/CyberIntell
*
* @author       José Pina Miranda - Devise Futures, Lda. (jose.miranda@devisefutures.com)
* @copyright    2017, Devise Futures Lda.
* @version      0.4
* @package      CyberIntell
* @subpackage   Twitter
* @since        0.4
* @license      https://gitlab.com/devisefutures/CyberIntell/blob/master/LICENSE.md  GNU General Public License v3.0
*/

# ----- config init -----
# ----- config end -----


/**
 * Função indicada no ficheiro de configuração de meaning.
 *
 * @since 0.3
 *
 * @param  string $row  linha do ficheiro de output de process (process.php).
 * @param  string $csvh header do ficheiro de output de process (process.php).
 * @param  string $args lista de argumentos para esta função indicados no ficheiro de configuração de meaning.
 * @return string      elemento a escrever na coluna do ficheiro de output de meaning (meaning.php).
 */
function created($row, $csvh, $args)
{
    $date = date_create_from_format("D M d H:i:s O Y", str_replace('"', '', $row[$csvh[$args[0]]]));
    return '"' . date_format($date, "Y/m/d H:i:s") . '"';
}

/**
 * Função indicada no ficheiro de configuração de meaning.
 *
 * @since 0.3
 *
 * @param  string $row  linha do ficheiro de output de process (process.php).
 * @param  string $csvh header do ficheiro de output de process (process.php).
 * @param  string $args lista de argumentos para esta função indicados no ficheiro de configuração de meaning.
 * @return string      elemento a escrever na coluna do ficheiro de output de meaning (meaning.php).
 */

function text($row, $csvh, $args)
{
    $retweet = str_replace('"', '', $row[$csvh[$args[3]]]);
    $original = str_replace('"', '', $row[$csvh[$args[4]]]);
    $source = str_replace('"', '', $row[$csvh[$args[5]]]);
    $favorites = str_replace('"', '', $row[$csvh[$args[6]]]);
    $text = str2twuser(str2hashtag(str2link(str_replace('"', '', $row[$csvh[$args[0]]]))));
    $ret = "<a href='https://twitter.com/" . str_replace('"', '', $row[$csvh[$args[1]]]) . "/status/" .
            str_replace('"', '', $row[$csvh[$args[2]]]) . "' target='_blank'><img src='https://cdn1.iconfinder.com/data/icons/logotypes/32/twitter-32.png' width='32' height='32'></a>" .$text;
    if ($original != "") {
        $ret .= " [<a href='$original' target='_blank'>Mais info</a>]";
    }
//    return '"' . $ret . "<br>[Re-tweets: $retweet / Favoritos (Likes): $favorites]<br>Notícia enviada a partir de: $source" . '"';
    return '"' . $ret . '"';
}


/**
 * Função indicada no ficheiro de configuração de meaning.
 *
 * @since 0.5
 *
 * @param  string $row  linha do ficheiro de output de process (process.php).
 * @param  string $csvh header do ficheiro de output de process (process.php).
 * @param  string $args lista de argumentos para esta função indicados no ficheiro de configuração de meaning.
 * @return string      elemento a escrever na coluna do ficheiro de output de meaning (meaning.php).
 */
function listdescription($row, $csvh, $args)
{
    //"description;member_count;mode;name;subscriber_count;uri"
    $description = str_replace('"', '', $row[$csvh[$args[0]]]);
    $members = str_replace('"', '', $row[$csvh[$args[1]]]);
    $mode = str_replace('"', '', $row[$csvh[$args[2]]]);
    $name = str2twuser(str2hashtag(str2link(str_replace('"', '', $row[$csvh[$args[3]]]))));
    $subscribers = str_replace('"', '', $row[$csvh[$args[4]]]);
    $uri = str_replace('"', '', $row[$csvh[$args[5]]]);
    $hts = "<a href='https://twitter.com" . $uri . "' target='_blank'>" . "$name</a>";

    return '"' . "$hts - $description [Tipo: $mode]<br>[Membros: $members / Subscritores: $subscribers]" . '"';
}


/**
 * Função indicada no ficheiro de configuração de meaning.
 *
 * @since 0.5
 *
 * @param  string $row  linha do ficheiro de output de process (process.php).
 * @param  string $csvh header do ficheiro de output de process (process.php).
 * @param  string $args lista de argumentos para esta função indicados no ficheiro de configuração de meaning.
 * @return string      elemento a escrever na coluna do ficheiro de output de meaning (meaning.php).
 */
function fulluserwithphoto($row, $csvh, $args)
{
    //username;user_location;user_screenname;user_description;user_followers;user_friends;user_favorites;user_tweets;user_id;user_created_at;user_image
    $username = str_replace('"', '', $row[$csvh[$args[0]]]);
    $local = str_replace('"', '', $row[$csvh[$args[1]]]);
    $screenname = str_replace('"', '', $row[$csvh[$args[2]]]);
    $desc = str2twuser(str2hashtag(str2link(str_replace('"', '', $row[$csvh[$args[3]]]))));
    $followers = str_replace('"', '', $row[$csvh[$args[4]]]);
    $friends = str_replace('"', '', $row[$csvh[$args[5]]]);
    $favorites = str_replace('"', '', $row[$csvh[$args[6]]]);
    $tweets = str_replace('"', '', $row[$csvh[$args[7]]]);
    $userid = str_replace('"', '', $row[$csvh[$args[8]]]);
    $created = str_replace('"', '', $row[$csvh[$args[9]]]);
    $userimage = str_replace('"', '', $row[$csvh[$args[10]]]);

    $hts = "<a href='https://twitter.com/" . $screenname . "' target='_blank'>";
//    return '"' . "<img src='" . $userimage . "' width='48' height='48'></a>$username - ID $userid - ($hts@$screenname</a>)<br>$desc<br>$local<br>" .
//        "[Seguidores: $followers / A seguir: $friends] [Tweets: $tweets / Favoritos: $favorites]<br>Criado em: $created" . '"';
    return '"' . "<img src='" . $userimage . "' width='48' height='48'></a>$username - ID $userid - ($hts@$screenname</a>)<br>$desc<br>$local<br>" .
            "Criado em: $created" . '"';
}


/**
 * Função indicada no ficheiro de configuração de meaning.
 *
 * @since 0.3
 *
 * @param  string $row  linha do ficheiro de output de process (process.php).
 * @param  string $csvh header do ficheiro de output de process (process.php).
 * @param  string $args lista de argumentos para esta função indicados no ficheiro de configuração de meaning.
 * @return string      elemento a escrever na coluna do ficheiro de output de meaning (meaning.php).
 */
function fulluser($row, $csvh, $args)
{
    //username;user_location;user_screenname;user_description;user_followers;user_friends;user_favorites;statuses_count;id_str
    $username = str_replace('"', '', $row[$csvh[$args[0]]]);
    $local = str_replace('"', '', $row[$csvh[$args[1]]]);
    $screenname = str_replace('"', '', $row[$csvh[$args[2]]]);
    $desc = str2twuser(str2hashtag(str2link(str_replace('"', '', $row[$csvh[$args[3]]]))));
    $followers = str_replace('"', '', $row[$csvh[$args[4]]]);
    $friends = str_replace('"', '', $row[$csvh[$args[5]]]);
    $favorites = str_replace('"', '', $row[$csvh[$args[6]]]);
    $tweets = str_replace('"', '', $row[$csvh[$args[7]]]);
    $userid = str_replace('"', '', $row[$csvh[$args[8]]]);
    $hts = "<a href='https://twitter.com/" . $screenname . "' target='_blank'>";
    return '"' . "$username - ID $userid - ($hts@$screenname</a>)<br>$desc<br>$local<br>" .
        "[Seguidores: $followers / A seguir: $friends] [Tweets: $tweets / Favoritos: $favorites]" . '"';
}

/**
 * Função indicada no ficheiro de configuração de meaning.
 *
 * @since 1.1
 *
 * @param  string $row  linha do ficheiro de output de process (process.php).
 * @param  string $csvh header do ficheiro de output de process (process.php).
 * @param  string $args lista de argumentos para esta função indicados no ficheiro de configuração de meaning.
 * @return string      elemento a escrever na coluna do ficheiro de output de meaning (meaning.php).
 */
function user($row, $csvh, $args)
{
    //username;user_location;user_screenname;user_description;id_str
    $username = str_replace('"', '', $row[$csvh[$args[0]]]);
    $local = str_replace('"', '', $row[$csvh[$args[1]]]);
    $screenname = str_replace('"', '', $row[$csvh[$args[2]]]);
    $desc = str2twuser(str2hashtag(str2link(str_replace('"', '', $row[$csvh[$args[3]]]))));
    $userid = str_replace('"', '', $row[$csvh[$args[4]]]);
    $hts = "<a href='https://twitter.com/" . $screenname . "' target='_blank'>";
    return '"' . "$username - ID $userid - ($hts@$screenname</a>)<br>$desc<br>$local<br>" . '"';
}

/**
 * Função indicada no ficheiro de configuração de meaning.
 *
 * @since 1.1
 *
 * @param  string $row  linha do ficheiro de output de process (process.php).
 * @param  string $csvh header do ficheiro de output de process (process.php).
 * @param  string $args lista de argumentos para esta função indicados no ficheiro de configuração de meaning.
 * @return string      elemento a escrever na coluna do ficheiro de output de meaning (meaning.php).
 */
function userdata($row, $csvh, $args)
{
    //user_followers;user_friends;user_favorites;statuses_count
    $followers = str_replace('"', '', $row[$csvh[$args[0]]]);
    $friends = str_replace('"', '', $row[$csvh[$args[1]]]);
    $favorites = str_replace('"', '', $row[$csvh[$args[2]]]);
    $tweets = str_replace('"', '', $row[$csvh[$args[3]]]);
    return '"' . "[Seguidores: $followers / A seguir: $friends] [Tweets: $tweets / Favoritos: $favorites]" . '"';
}


/**
 * Função indicada no ficheiro de configuração de meaning.
 *
 * @since 0.3
 *
 * @param  string $row  linha do ficheiro de output de process (process.php).
 * @param  string $csvh header do ficheiro de output de process (process.php).
 * @param  string $args lista de argumentos para esta função indicados no ficheiro de configuração de meaning.
 * @return string      elemento a escrever na coluna do ficheiro de output de meaning (meaning.php).
 */
function foto($row, $csvh, $args)
{
    $userimage = str_replace('"', '', $row[$csvh[$args[0]]]);
    return '"' . "<img src='" . $userimage . "' width='48' height='48'></a>" . '"';
}
