<?php
/**
* CyberIntell - feed e aplicação capaz de recolher dados e monitorizar/alertar
*              a ocorrência de dados/ameaças.
*
* process.php
*     Processa os dados recolhidos do twitter através do gather.php e, cria ficheiro
*     único processado a partir dos vários ficheiros recolhidos no gather para cada
*     um dos identificadores de pesquisa
*
* @link   https://gitlab.com/devisefutures/CyberIntell
*
* @author       José Pina Miranda - Devise Futures, Lda. (jose.miranda@devisefutures.com)
* @copyright    2017, Devise Futures Lda.
* @version      0.4
* @package      CyberIntell
* @subpackage   Twitter
* @since        0.4
* @license      https://gitlab.com/devisefutures/CyberIntell/blob/master/LICENSE.md  GNU General Public License v3.0
*/

# ----- config init -----
/**
  * @since 0.4
  * @var string CONFIG  Path para ficheiro de configurações.
  */
const CONFIG = "twitter.process.conf";
 /**
  * @since 0.4
  * @var string GENERAL_SECTION  Nome da secção geral na CONFIG
  */
const GENERAL_SECTION = "general";
  /**
   * @since 0.4
   * @var string OUTPUT_DIR Nome da diretoria de output na GENERAL_SECTION
   */
const OUTPUT_DIR = "output";
  /**
   * @since 0.4
   * @var string INPUT_DIR Nome da diretoria de info de processamento na GENERAL_SECTION
   */
const INPUT_DIR = "input";
/**
 * @since 0.6
 * @var string VERSION Versão da aplicação
 */
const VERSION = '1.0';
# ----- config end -----

/**
 * funções genéricas
 */
require_once('misc.php');
require_once('pp_functions.php');

/**
 * Processa as pesquisas recolhidas, de acordo com as secções de configuração de tipo de pesquisas
 * no ficheiro $config. Note-se que $gsection é uma secção genérica de configuração.
 *
 * @since 0.4
 *
 * @param  string $config   Ficheiro com secções de configuração de tipo de pesquisas.
 * @param  string $gsection Secção de configuração genérica em $config.
 * @return bool             true, no caso de conseguir efetuar normalização do tipo de pesquisas.
 */
function process($config = CONFIG, $gsection = GENERAL_SECTION)
{
    $ageneral = get_conf_section($gsection, $config);
    if (empty($ageneral) || !array_key_exists(OUTPUT_DIR, $ageneral) || !array_key_exists(INPUT_DIR, $ageneral)) {
        fwrite(STDERR, "Warning: Section $gsection (in file $config) lacks proper configuration.\n");
        return false;
    } elseif (!create_dir($ageneral[OUTPUT_DIR]) || !create_dir($ageneral[INPUT_DIR])) {
        return false;
    }
    $sections = get_sections_name($config, [GENERAL_SECTION]);
    if (empty($sections)) {
        fwrite(STDERR, "Warning: Could not find search type in file $config.\n");
        return false;
    }
    foreach ($sections as $sec) {
        // ver quais são os IDs de pesquisa e processar para cada um deles.
        foreach (searchIDs(glob($ageneral[INPUT_DIR].$sec.".*.json")) as $searchID) {
            $afile = glob($ageneral[OUTPUT_DIR].$sec.".".$searchID.".*.csv");
            $f2p = files2process(glob($ageneral[INPUT_DIR].$sec.".".$searchID.".*.json"), $afile);
            if (!empty($f2p)) {
                $outf = str_replace(
                    [$ageneral[INPUT_DIR], ".json"],
                    [$ageneral[OUTPUT_DIR], ".csv"],
                    $f2p[count($f2p) - 1]
                );
                $conf = get_conf_section($sec, $config);
                processSearchID($f2p, $afile, $outf, $conf['n']);
            }
        }
    }
    return true;
}


/**
 * Efectua pós-processamento após a execução de process
 *
 * @since 1.2
 *
 * @param  string $config   Ficheiro com secções de configuração de pós-processamento.
 * @param  string $gsection Secção de configuração genérica em $config.
 * @return bool             true, no caso de conseguir efetuar pós-procesammento.
 */
function postProcess($config, $gsection = GENERAL_SECTION)
{
    $ageneral = get_conf_section($gsection, $config);
    if (empty($ageneral) || !array_key_exists(INPUT_DIR, $ageneral)) {
        fwrite(STDERR, "PP Warning: Section $gsection (in file $config) lacks proper configuration.\n");
        return false;
    } elseif (!create_dir($ageneral[INPUT_DIR])) {
        return false;
    }
    $sections = get_sections_name($config, [GENERAL_SECTION]);
    if (empty($sections)) {
        fwrite(STDERR, "PP Warning: Could not find search type in file $config.\n");
        return false;
    }
    foreach ($sections as $sec) {
        $file = glob($ageneral[INPUT_DIR].$sec.".*.csv");
        if (empty($file) || !$file) {
            fwrite(STDERR, "PP Warning: Could not find $sec file for post-processing.\n");
            continue;
        }
        $file = $file[0];
        $conf = get_conf_section($sec, $config);
        ppRules($file, $conf);
    }
    return true;
}



/**
 * Aplica as regras $aconf $file.
 *
 * @since 1.2
 *
 * @param  string $file ficheiro a processar
 * @param  array $aconf array de regras (identificadas na configuração de tipo de pesquisas) a aplicar a $file
 * @return void
 */
function ppRules($file, $aconf)
{
    if (empty($aconf)) {
        fwrite(STDERR, "PP Warning: No rules for post-processing file $file.\n");
        return;
    }
    $ofile = "/tmp/pp" . pathinfo($file, PATHINFO_FILENAME) . time();
    $fpi = fopen($file, 'r');
    $fpo = fopen($ofile, 'w');
    $csvheader = fgets($fpi);
    fwrite($fpo, $csvheader);
    $csvheader = array_flip(str_replace(['"', "\n"], ['',''], explode(';', $csvheader)));
    $ref = []; // variavel a ser passada por referência às funções de pp_functions.php

    while ($line = fgets($fpi)) {
        $row = explode(';', $line);
        $w2o = true;
        foreach (array_keys($aconf) as $val) {
            if (!array_key_exists($aconf[$val]['header'], $csvheader)) {
                fwrite(STDERR, "PP Warning: Header " . $aconf[$val]['header'] . " in rule $val.\n");
                continue;
            }
            if (!$w2o ||
                !$aconf[$val]['func']($row[$csvheader[$aconf[$val]['header']]], explode(';', $aconf[$val]['args']), $ref)) {
                $w2o = false;
            }
        }
        $w2o ? fwrite($fpo, $line) : false;
    }
    fclose($fpi);
    fclose($fpo);
    `mv $ofile $file`;
}




/**
 * Processa o identificador de pesquisa
 *
 * @since  0.4
 *
 * @param  array $fileList  Lista de ficheiros de pesquisa do identificador de pesquisa.
 * @param  array $afile     Lista de ficheiros (máximo de 1), com ficheiro normalizado
 *                          anteriormente, relativo ao identificador de pesquisa.
 * @param  string $outfile  Novo nome do ficheiro normalizado.
 * @param  array $aconf     Regras de normalização.
 * @return bool             true, no caso de conseguir efetuar normalização do identificador de pesquisa.
 */
function processSearchID($fileList, $afile, $outfile, $aconf)
{
    if (empty($fileList)) {
        return true;
    }
    if (empty($aconf)) {
        fwrite(STDERR, "Warning: No normalization rules for output file $outfile.\n");
        return true;
    }
    if (empty($afile)) {
        if (!file_put_contents($outfile, '"'.implode('";"', array_keys($aconf)).'"'."\n")) {
            fwrite(STDERR, "Error: unable to write normalized output to $outfile.\n");
            return false;
        }
    }
    if (!empty($afile) && !rename($afile[0], $outfile)) {
        fwrite(STDERR, "Error: unable to rename $afile[0] into $outfile.\n");
        return false;
    }
    $fpo = fopen($outfile, 'a+');
    fseek($fpo, 0);
    $csvheader = str_replace(['"', "\n"], ['',''], explode(';', fgets($fpo)));

    foreach ($fileList as $file) {
        $json = json_decode(file_get_contents($file), true);
        $json = getrealelems(['statuses', 'users', 'lists'], $json);
        foreach ($json as $status) {
            foreach ($csvheader as $elem) {
                $campo = get_elem($status, $aconf[$elem]);
                if (!$campo) {
                    $campo = '';
                }
                fwrite($fpo, '"'.str_replace(['"', ";", "\n"], ["'", ",", " "], $campo).'";');
            }
            fwrite($fpo, "\n");
        }
    }
    fclose($fpo);
    return true;
}


/**
 * Devolve os elementos do $json a processar que estiverem associados a uma das $keys (a que existir). Caso
 * não exista nenhuma das $keys no $json, devolve $json.
 * @param  array $keys Keys a pesquisar no $json.
 * @param  array $json json de resposta de gathering.
 * @param  string $except Se não encontrar $keys no $json, valida se a key $except existe no $json
 * @return string       (i) json associado a uma das $keys, ou (ii) [] se (i) não existir e key $excpet existe no $json,
 *                          ou (iii) caso (i) e (ii) não se verifiquem.
 */
function getrealelems($keys, $json, $except = "errors")
{
    foreach ($keys as $key) {
        if (array_key_exists($key, $json)) {
            return $json[$key];
        }
    }
    if (array_key_exists($except, $json)) {
        return [];
    }
    if (count(array_filter(array_keys($json), 'is_string')) > 0) {
        return [$json];
    }
    return $json;
}



/**
 * Função de execução do programa
 * @param  string $options  Similar ao primeiro argumento de getopt().
 * @param  array $longopts  Similar ao segundo argumento de getopt().
 * @param  int $nparams     Número de argumentos obrigatórios da linha de comando + 1.
 * @param  array $arg   Argumentos da linha de comando (deverá ser $argv).
 * @return int
 */
function main($options, $longopts, $nparams, $arg)
{
    $aopt = getopt($options, $longopts);
    if (empty($aopt) && count($arg) > $nparams) {
        fwrite(STDOUT, "Usage: php $arg[0] [--h] [--v] [-f <config filename>] [-p <config filename>]\n");
        return 0;
    } elseif (array_key_exists("h", $aopt)) {
        fwrite(STDOUT, "Usage: php $arg[0] [--h] [--v] [-f <config filename>] [-p <config filename>]\n");
        fwrite(STDOUT, "   --h \t\thelp\n");
        fwrite(STDOUT, "   --v \t\tversion\n");
        fwrite(STDOUT, "   -f \t\tprocessing configuration file\n");
        fwrite(STDOUT, "   -p \t\tpost-processing configuration file\n");
        fwrite(STDOUT, "\n$arg[0] - Process previously gathered data from Twitter, according to config files.\n\n");
        return 0;
    } elseif (array_key_exists("v", $aopt)) {
        fwrite(STDOUT, "$arg[0] \nVersion:" . VERSION. "\n");
        return 0;
    }
    (array_key_exists("f", $aopt)) ? process($aopt["f"]) : process();
    (array_key_exists("p", $aopt)) ? postProcess($aopt["p"]) : true;
    return 0;
}

exit(main("f:p:", ["h", "v"], 1, $argv));
