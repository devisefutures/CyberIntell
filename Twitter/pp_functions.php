<?php
/**
* CyberIntell - feed e aplicação capaz de recolher dados e monitorizar/alertar
*              a ocorrência de dados/ameaças.
*
* pp_functions.php
*     Funções disponíveis para serem indicadas no ficheiro de configuração de post-processing.
*
* @link   https://gitlab.com/devisefutures/CyberIntell
*
* @author       José Pina Miranda - Devise Futures, Lda. (jose.miranda@devisefutures.com)
* @copyright    2017, Devise Futures Lda.
* @version      1.2
* @package      CyberIntell
* @subpackage   Twitter
* @since        1.2
* @license      https://gitlab.com/devisefutures/CyberIntell/blob/master/LICENSE.md  GNU General Public License v3.0
*/

# ----- config init -----
# ----- config end -----


/**
 * Função indicada no ficheiro de configuração de post-processing. Verifica se $col inclui todas as palavras em $args
 *
 * @since 1.2
 *
 * @param  string $col  coluna a pos-processar.
 * @param  string $args lista de argumentos para esta função indicados no ficheiro de configuração de post-processing.
 * @param array $ref argumento passado por referência, a ser utilizado se a função precisar de aceder a resultados
 *                   referentes a outras linhas processadas previamente.
 * @return bool      verdadeiro, se a coluna $col tem as propriedades que esta função valida.
 */
function pptextIncludeAllWords($col, $args, &$ref)
{
    $ret = true;
    foreach ($args as $word) {
        $ret = $ret && containsWord($col, $word);
    }
    return $ret;
}


/**
 * Função indicada no ficheiro de configuração de post-processing. Verifica se $col inclui uma das palavras em $args
 *
 * @since 1.3
 *
 * @param  string $col  coluna a pos-processar.
 * @param  string $args lista de argumentos para esta função indicados no ficheiro de configuração de post-processing.
 * @param array $ref argumento passado por referência, a ser utilizado se a função precisar de aceder a resultados
 *                   referentes a outras linhas processadas previamente.
 * @return bool      verdadeiro, se a coluna $col tem as propriedades que esta função valida.
 */
function pptextIncludeOneWord($col, $args, &$ref)
{
    foreach ($args as $word) {
        if (containsWord($col, $word)) {
            return true;
        }
    }
    return false;
}


/**
 * Função indicada no ficheiro de configuração de post-processing. Verifica se $col é igual a uma $col previamente
 * processada.
 *
 * @since 1.3
 *
 * @param  string $col  coluna a pos-processar.
 * @param  string $args lista de argumentos para esta função indicados no ficheiro de configuração de post-processing.
 * @param array $ref argumento passado por referência, a ser utilizado se a função precisar de aceder a resultados
 *                   referentes a outras linhas processadas previamente.
 * @return bool      verdadeiro, se a coluna $col tem as propriedades que esta função valida.
 */
function remDupCol($col, $args, &$ref)
{
    if (!array_key_exists("remDupCol", $ref)) { // a variavel ref é partilhada entre as funções que necessitarem de
                // fazer passagem de argumentos por referência, pelo que deverá ocupar/processar o seu próprio sub-array
        $ref["remDupCol"] = [];
    }
    $hash = hash("sha1", $col); // assumo que neste contexto sha1 é mais do que suficiente
    if (in_array($hash, $ref["remDupCol"])) {
        return false;
    }
    $ref["remDupCol"][] = $hash;
    return true;
}


/**
 * Função indicada no ficheiro de configuração de post-processing. Verifica se $col é igual a uma $col previamente
 * processada, não tendo em conta os URL que possam estar em $col.
 *
 * @since 1.3
 *
 * @param  string $col  coluna a pos-processar.
 * @param  string $args lista de argumentos para esta função indicados no ficheiro de configuração de post-processing.
 * @param array $ref argumento passado por referência, a ser utilizado se a função precisar de aceder a resultados
 *                   referentes a outras linhas processadas previamente.
 * @return bool      verdadeiro, se a coluna $col tem as propriedades que esta função valida.
 */
function remDupColnoURL($col, $args, &$ref)
{
    if (!array_key_exists("remDupColnoURL", $ref)) {
        $ref["remDupColnoURL"] = [];
    }
    $col = preg_replace('@((https?://)?([-\w]+\.[-\w\.]+)+\w(:\d+)?(/([-\w/_\.]*(\?\S+)?)?)*)@', "", $col);
    $hash = hash("sha1", $col); // assumo que neste contexto sha1 é mais do que suficiente
    if (in_array($hash, $ref["remDupColnoURL"])) {
        return false;
    }
    $ref["remDupColnoURL"][] = $hash;
    return true;
}


/**
 * Função indicada no ficheiro de configuração de post-processing. Verifica se $col é igual a uma $col previamente
 * processada, tendo em conta o facto de ser um retweet de um $col anterior.
 *
 * @since 1.3
 *
 * @param  string $col  coluna a pos-processar.
 * @param  string $args lista de argumentos para esta função indicados no ficheiro de configuração de post-processing.
 * @param array $ref argumento passado por referência, a ser utilizado se a função precisar de aceder a resultados
 *                   referentes a outras linhas processadas previamente.
 * @return bool      verdadeiro, se a coluna $col tem as propriedades que esta função valida.
 */
function remDupColnoRetweet($col, $args, &$ref)
{
    if (!array_key_exists("remDupColnoRetweet", $ref)) {
        $ref["remDupColnoRetweet"] = [];
    }
    $col = preg_replace('@RT \@(\w+): @', "", $col);
    $hash = hash("sha1", $col); // assumo que neste contexto sha1 é mais do que suficiente
    if (in_array($hash, $ref["remDupColnoRetweet"])) {
        return false;
    }
    $ref["remDupColnoRetweet"][] = $hash;
    return true;
}



/**
 * Verifica se $str contém $word
 *
 * @since 1.2
 * @param  string $str
 * @param  string $word
 * @return bool       true se $word contiver $str; false caso contrário
 */
function containsWord($str, $word)
{
    return !!preg_match('#\\b' . preg_quote($word, '#') . '\\b#i', $str);
}
