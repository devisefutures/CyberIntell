# CyberIntell Twitter - Análise Crítica

## 1. CyberIntell Twitter

O CyberIntell Twitter é uma implementação de uma feed e aplicação capaz de recolher dados e monitorizar/alertar para a ocorrência de dados/ameaças
no [twitter](https://twitter.com/).

Está organizada em pequenas aplicações de acordo com o *Threat Intelligence Life Cycle*:
+ Gather - aplicação `gather.php` que vai buscar os dados de acordo com as pesquisas de dados/ameaças a efectuar;
+ Process - aplicação `process.php` que processa e normaliza os dados obtidos na fase anterior;
+ Create Meaning - aplicação `meaning.php` que pega nos dados processados e os filtra e agrupa/separa, de modo a serem mais facilmente analisados pelos analistas;
+ Consumption - aplicação `consumption.php` que gera informação adicional, de modo a que os dados obtidos na fase anterior possam ser apresentados aos analistas num ambiente ou aplicação de visualização.

O objetivo é poder ser instalada numa plataforma extremamente low-cost (máquina 4-core ARMv7 CPU com 2GB de RAM
e 50 GB SSD) que possa ser utilizada pelas PME e micro-empresas, assim como em plataforma baseada numa [stack ELK][elk].


## 2. Análise crítica

A solução CyberIntell Twitter implementada garante a maior flexibilidade na obtenção, tratamento e visualização dos dados pesquisados no Twitter, permitindo
o seu funcionamento numa plataforma extremamente low-cost (máquina 4-core ARMv7 CPU com 2GB de RAM e 50 GB SSD) que pode ser utilizada pelas PME e micro-empresas
nas suas operações de *Cyber-intelligence*.

## 2.1 API Twitter

A versão actual (1.1) das APIs do Twitter permitem efectuar pesquisas com grande flexibilidade, tendo apenas a apontar:
+ Limite no ritmo de pesquisa - as várias pesquisas não podem ser chamadas a um ritmo mais elevado do que uma pesquisa por API em cada 15 minutos -. Este limite pode ser ultrapassado caso se registem várias aplicações e utilizadores;
+ Nem todas as APIs utilizam os mecanismos que permitem optimizar as pesquisas nas respostas do Twitter ('next_cursor' e 'refresh_url');
+ Existem dois mecanismos diferentes de optimização de pesquisa nas respostas do Twitter ('next_cursor' e 'refresh_url'), que fazem essencialmente o mesmo.

No desenvolvimento da solução foram encontrados alguns problemas, principalmente relacionados com a API do PasteBin. A versão grátis da API do PasteBin tem algumas limitações, como por exemplo:

## 2.2 csv-to-html-table e huge

A utilização do [csv-to-html-table](https://github.com/derekeder/csv-to-html-table) e [huge](https://github.com/panique/huge) auxiliou na visualização
da informação, não sendo necessário investir muito tempo nessa componente.

Com o aumento do volume de informação recolhida, e como no [csv-to-html-table](https://github.com/derekeder/csv-to-html-table) a 'geração' da visualização
é efectuada pelo javascript do lado do browser, poderá começar a existir demora na apresentação da mesma (mais tempo, consoante menos memória e poder de processamento
tiver o computador do analista).

## 2.3 Plataforma CyberIntell Twitter

A [plataforma CyberIntell Twitter](https://cyberintell.projj.eu/) corre em hardware "low-cost" - máquina 4-core ARMv7 CPU com 2GB de RAM e 50 GB SSD -, tal como era o objectivo inicial. Não julgamos que a mesma venha a ter qualquer constrangimento, a não ser a nível do disco dependendo do tipo de pesquisas efectuadas.
Contudo, mesmo com esse potencial constrangimento, a solução parece adequada para operações de *Cyber-intelligence* a efectuar por uma PME ou micro-empresa.

## 2.4 Evoluções futuras

O CyberIntell Twitter deverá evoluir no seguinte sentido:
+ Melhoria do aspecto da visualização dos dados pelo analista;
+ Melhoria nas configurações, de modo a ser possível correlacionar automaticamente a resposta de várias APIs;
+ Introdução de inteligência, de modo a ser possível tomar decisões automáticas de com base na resposta de uma API, efectuar pesquisa com outra(s) API(s);
+ Introduzir novos tipos de visualização que permitissem, por exemplo, que o analista pudesse facilmente perceber a inter-relação entre utilizadores e se possível detectas automaticamente padrões nessas relações;
+ Introdução de alertas (por email, SMS, Slack, ...) na deteção de inter-relações ou padrões de tweets pré-definidos.

Em relação à [plataforma CyberIntell Twitter](https://cyberintell.projj.eu/), a próxima versão deverá permitir a cada utilizador definir e aceder apenas
às suas pesquisas.

## 3. Conclusão

Será necessário algum tempo para se perceber se um analista consegue utilizar esta plataforma para recolher informação relevante para as operações de
*Cyber-intelligence* da sua entidade.
É isso que vai ser verificado nos próximos tempos, tendo sido pedido a alguns colegas da Pós-Graduação para fornecerem as pesquisas que querem efectuar e irem
verificando se a informação obtida é relevante, assim como alargando/refinando a pesquisa.
