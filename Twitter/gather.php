<?php
/**
* CyberIntell - feed e aplicação capaz de recolher dados e monitorizar/alertar
*              a ocorrência de dados/ameaças.
*
* gather.php
*     Recolhe dados do twitter.
*
* @link   https://gitlab.com/devisefutures/CyberIntell
*
* @author       José Pina Miranda - Devise Futures, Lda. (jose.miranda@devisefutures.com)
* @copyright    2017, Devise Futures, Lda.
* @version      0.3
* @package      CyberIntell
* @subpackage   Twitter
* @since        0.1
* @license      https://gitlab.com/devisefutures/CyberIntell/blob/master/LICENSE.md  GNU General Public License v3.0
*/

# ----- config init -----
/**
 * @since 0.1
 * @var string CONFIG_FILE  Path para ficheiro de configurações necessárias para recolha de dados.
 */
const CONFIG_FILE = "twitter.conf";
/**
  * @since 0.1
  * @var string TWITTER_SECTION Nome da secção twitter na CONFIG_FILE
  */
 const TWITTER_SECTION = "gather";
 /**
  * @since 0.1
  * @var string OAUTH_ACCESS_TOKEN Nome do oauth_access_token na TWITTER_SECTION
  */
 const OAUTH_ACCESS_TOKEN = "oauth_access_token";
 /**
  * @since 0.1
  * @var string OAUTH_ACCESS_TOKEN_SECRET Nome do oauth_access_token_secret na TWITTER_SECTION
  */
 const OAUTH_ACCESS_TOKEN_SECRET = "oauth_access_token_secret";
 /**
  * @since 0.1
  * @var string CONSUMER_KEY Nome do consumer_key na TWITTER_SECTION
  */
 const CONSUMER_KEY = "consumer_key";
 /**
  * @since 0.1
  * @var string CONSUMER_SECRET Nome do consumer_secret na TWITTER_SECTION
  */
 const CONSUMER_SECRET = "consumer_secret";
/**
  * @since 0.1
  * @var string CONFIG_SEARCH  Path para ficheiro de configurações para pesquisa de dados.
  */
 const CONFIG_SEARCH = "twitter.gather.conf";
/**
 * @since 0.1
 * @var string GENERAL_SECTION  Nome da secção e "sub-secção" geral na CONFIG_SEARCH
 */
 const GENERAL_SECTION = "general";
 /**
  * @since 0.1
  * @var string OUTPUT_DIR Nome da diretoria de output na GENERAL_SECTION
  */
 const OUTPUT_DIR = "output";
 /**
  * @since 0.1
  * @var string PROC_DIR Nome da diretoria de info de processamento na GENERAL_SECTION
  */
 const PROC_DIR = "proc";
 /**
  * @since 0.4
  * @var string URL_SEARCH Nome do identificador do URL de pesquisa
  */
 const URL_SEARCH = "url";
/**
 * @since 0.4
 * @var string REQUIRED_FIELD Nome do identificador parâmetro obrigatório
 */
const REQUIRED_FIELD = "required";
/**
 * @since 0.4
 * @var string NONE Nome do identificador do parâmetro que não deve ser utilizado
 */
const NONE = 'none';
/**
 * @since 0.6
 * @var string VERSION Versão da aplicação
 */
const VERSION = '1.0';
# ----- config end -----

/**
* j7mbo/twitter-api-php
*/
require_once('vendor/autoload.php');
/**
 * funções genéricas
 */
require_once('misc.php');

/**
 * Pesquisa dados no twitter
 *
 * Pesquisa dados no twitter (pesquisa search/tweets), de acordo com configurações efectuadas
 * na CONFIG_SEARCH e CONFIG_FILE
 *
 * @since 0.1
 *
 * @param  string $configSearch  Optional. Path para o ficheiro de configuração de pesquisa de dados. Default
 *                               CONFIG_SEARCH.
 * @param  mixed $tsection  Optional. Secção de configuração de tokens para acesso ao twitter na
 *                          $configFile. Default TWITTER_SECTION.
 * @param  mixed $gsection  Optional. Secção de configuração geral na $configSearch. Default GENERAL_SECTION.
 * @param  string $configFile  Optional. Path para o ficheiro de configuração de autenticações. Default CONFIG_FILE.
 * @return bool FALSE, caso exista erro. TRUE, caso contrário.
 */
function gather_searchtweets(
    $configSearch = CONFIG_SEARCH,
    $tsection = TWITTER_SECTION,
    $gsection = GENERAL_SECTION,
    $configFile = CONFIG_FILE
) {
    $atoken = get_conf_section($tsection, $configFile);
    if (empty($atoken) || !array_key_exists(OAUTH_ACCESS_TOKEN, $atoken) ||
            !array_key_exists(OAUTH_ACCESS_TOKEN_SECRET, $atoken) || !array_key_exists(CONSUMER_KEY, $atoken) ||
            !array_key_exists(CONSUMER_SECRET, $atoken)) {
        fwrite(STDERR, "Warning: Section $tsection (in file $configSearch) lacks proper configuration.\n");
        return false;
    }
    $sections = get_sections_name($configSearch, [$gsection]);
    if (empty($sections)) {
        fwrite(STDERR, "Warning: File $configSearch lacks proper configuration.\n");
        return false;
    }
    foreach ($sections as $sect) {
        $asearch = process_searchconfig($sect, [$gsection], $configSearch);
        if (empty($asearch)) {
            fwrite(STDERR, "Warning: unable to search tweets - empty search query -.\n");
            return false;
        }
        $ageneral = get_conf_section($gsection, $configSearch);
        $subsgeneral = get_subsection($sect, [$gsection], $configSearch);
        if (empty($ageneral) || !array_key_exists(OUTPUT_DIR, $ageneral) || !array_key_exists(PROC_DIR, $ageneral) ||
                !array_key_exists($gsection, $subsgeneral) ||
                !array_key_exists(URL_SEARCH, $subsgeneral[$gsection])) {
            fwrite(STDERR, "Warning: Section $gsection (in file $configSearch) lacks proper configuration.\n");
            return false;
        } elseif (!create_dir($ageneral[OUTPUT_DIR]) || !create_dir($ageneral[PROC_DIR])) {
            return false;
        }
        $urlSearch = $subsgeneral[$gsection][URL_SEARCH];

        $twitter = new TwitterAPIExchange($atoken);

        foreach ($asearch as $key => $value) {
            $ofield = "";
            $getfield = getnextquery($ageneral[PROC_DIR].$sect.".".$key);
            $reqfield =  (array_key_exists(REQUIRED_FIELD, $subsgeneral[$gsection])) ?
                                $subsgeneral[$gsection][REQUIRED_FIELD] : false ;
            if (empty($getfield)) { // se não existe refresh_url
                foreach ($value as $k => $v) {
                    if ($reqfield && ($k == $reqfield)) {
                        $getfield = "?$k=$v";
                    } elseif ($k != NONE) {
                        $ofield .= "&$k=$v";
                    }
                }
            }
            if ($reqfield && empty($getfield)) {
                fwrite(STDERR, "Warning: unable to search tweets for search id $key - query not defined -.\n");
            } else {
                $jresults = $twitter->setGetfield($getfield.$ofield)->
                                        buildOauth($urlSearch, 'GET')->performRequest();
                $results = json_decode($jresults, true);
                if (empty($results)) {
                    fwrite(STDERR, "Warning: Empty response for search id $key.\n");
                } else {
                    writenextquery($results, $ageneral[PROC_DIR].$sect.".".$key, $getfield.$ofield);
                    if (!file_put_contents(
                        $ageneral[OUTPUT_DIR].$sect.".".$key.".".time().".json",
                        $jresults
                    )) {
                        fwrite(STDERR, "Warning: unable to write twitter response for search id $key.\n");
                    }
                }
            }
        }
    }
    return true;
}


/**
 * Verifica se existe ficheiro que contenha a query e informação sobre a partir de que ponto começar a próxima query. Se
 * tal acontecer, devolve a próxima query.
 *
 * @since 0.4
 *
 * @param  string $fname    Nome parcial do ficheiro onde se vai buscar a próxima query.
 * @return string        Próxima query.
 */
function getnextquery($fname)
{
    if (file_exists("$fname.refresh_url")) {
        return rawurldecode(file_get_contents("$fname.refresh_url"));
    }
    if (file_exists("$fname.next_cursor")) {
        return rawurldecode(file_get_contents("$fname.next_cursor"));
    }
    return "";
}


/**
 * Verifica se o resultado $result do gathering inclui informação sobre onde começar a próxima pesquisa
 * de modo a não se obterem resultados duplicados. Se isso acontecer, escreve a nova query para $fname
 * com a extensão adequada.
 *
 * @since 0.4
 *
 * @param  array $results   Resultado do gathering sob a forma de array.
 * @param  string $fname    Nome parcial do ficheiro onde se vai escrever a próxima query
 * @param  string $prevquery Query do gathering anterior.
 * @return void
 */
function writenextquery($results, $fname, $prevquery)
{
    if (array_key_exists("search_metadata", $results) &&
            array_key_exists("refresh_url", $results["search_metadata"])) {
        file_put_contents("$fname.refresh_url", $results["search_metadata"]["refresh_url"]);
    }
    if (array_key_exists("next_cursor_str", $results)) {
        $prevquery = ($prevquery[0] == '?') ? substr($prevquery, 1) : $prevquery ;
        parse_str($prevquery, $query);
        $nextc = $results["next_cursor_str"];
        $query['cursor']= ($nextc != "0") ? $nextc : "-1";
        file_put_contents("$fname.next_cursor", "?".http_build_query($query));
    }
}



/**
 * Função de execução do programa
 * @param  string $options  Similar ao primeiro argumento de getopt().
 * @param  array $longopts  Similar ao segundo argumento de getopt().
 * @param  int $nparams     Número de argumentos obrigatórios da linha de comando + 1.
 * @param  array $arg   Argumentos da linha de comando (deverá ser $argv).
 * @return int
 */
function main($options, $longopts, $nparams, $arg)
{
    $aopt = getopt($options, $longopts);
    if (empty($aopt) && count($arg) > $nparams) {
        fwrite(STDOUT, "Usage: php $arg[0] [--h] [--v] [-f <config filename>]\n");
        return 0;
    } elseif (array_key_exists("h", $aopt)) {
        fwrite(STDOUT, "Usage: php $arg[0] [--h] [--v] [-f <config filename>]\n");
        fwrite(STDOUT, "   --h \t\thelp\n");
        fwrite(STDOUT, "   --v \t\tversion\n");
        fwrite(STDOUT, "   -f \t\tgathering configuration file\n");
        fwrite(STDOUT, "\n$arg[0] - Gather data from Twitter, according to config file.\n\n");
        return 0;
    } elseif (array_key_exists("v", $aopt)) {
        fwrite(STDOUT, "$arg[0] \nVersion:" . VERSION. "\n");
        return 0;
    }
    (array_key_exists("f", $aopt)) ? gather_searchtweets($aopt["f"]) : gather_searchtweets();
    return 0;
}

exit(main("f:", ["h", "v"], 1, $argv));
