<?php
/**
* CyberIntell - feed e aplicação capaz de recolher dados e monitorizar/alertar
*              a ocorrência de dados/ameaças.
*
* misc.php
*     Miscelânea de funções a utilizar pelos outras várias componentes.
*
* @link   https://gitlab.com/devisefutures/CyberIntell
*
* @author     José Pina Miranda - Devise Futures, Lda. (jose.miranda@devisefutures.com)
* @copyright  2017, Devise Futures, Lda.
* @version    0.4
* @package    CyberIntell
* @subpackage Twitter
* @since    0.4
* @license  https://gitlab.com/devisefutures/CyberIntell/blob/master/LICENSE.md  GNU General Public License v3.0
*/


/**
 * Processa os parâmetros de pesquisa.
 *
 * Processa os parâmetros de pesquisa da secção $section do ficheiro $configSearch.
 *
 * @since 0.1
 *
 * @param string $section Secção com parâmetros de pesquisa
 * @param array $except Optional. Array com IDs da "sub-secção" que não fazem parte dos parâmetros de pesquisa e que
 *                      não serão devolvidos. Default [].
 * @param string $configSearch Optional. Path para o ficheiro com parâmetros de pesquisa. Default CONFIG_SEARCH.
 * @return [] ou array {
 *                      array identificador de pesquisa {
 *                                 mixed parâmetro de pesquisa => @type mixed valor associado ao parâmetro de pesquisa
 *                            }
 *               }
 */
function process_searchconfig($section, $except = [], $configSearch = CONFIG_SEARCH)
{
    if (!file_exists($configSearch)) {
        fwrite(STDERR, "Warning: unable to parse file $configSearch.\n");
        return [];
    }
    $arr = parse_ini_file($configSearch, true);
    if (!array_key_exists($section, $arr)) {
        fwrite(STDERR, "Warning: Section $section does not exist in file $configSearch.\n");
        return [];
    }
    $array = $arr[$section]; // Array da secção
    $rarray = [];
    foreach ($array as $key => $value) {
        foreach ($value as $k => $v) {
            if (!in_array($k, $except)) {
                $rarray[$k][$key] = $v;
            }
        }
    }
    return $rarray;
}


/**
 * Devolve "sub-secção" $subsection da secção $section do ficheiro de configuração $configFile.
 *
 * @since 0.4
 *
 * @param string $section    Secção cdo ficheiro de configuração.
 * @param array  $subsection "Sub-secção" da secção $section.
 * @param string $configFile  Path para o ficheiro de configuração.
 * @return [] ou array {
 *                      array identificador de subsecção {
 *                                 mixed parâmetro => mixed valor associado ao parâmetro
 *                            }
 *               }
 */
function get_subsection($section, $subsection, $configFile)
{
    if (!file_exists($configFile)) {
        fwrite(STDERR, "Warning: unable to parse file $configFile.\n");
        return [];
    }
    $arr = parse_ini_file($configFile, true);
    if (!array_key_exists($section, $arr)) {
        fwrite(STDERR, "Warning: Section $section does not exist in file $configFile.\n");
        return [];
    }
    $array = $arr[$section]; // Array da secção
    $rarray = [];
    foreach ($array as $key => $value) {
        foreach ($value as $k => $v) {
            if (in_array($k, $subsection)) {
                $rarray[$k][$key] = $v;
            }
        }
    }
    return $rarray;
}


/**
 * Devolve array com dados da secção
 *
 * Devolve um array com todos os dados na secção $section no ficheiro $configFile
 *
 * @since 0.1
 *
 * @param  mixed $section Secção da $config_file onde estão definidos os dados a devolver.
 * @param  string $configFile Path para o ficheiro de configuração.
 * @return array {
 *               mixed identificador => mixed valor associado ao identificador
 *         }
 */
function get_conf_section($section, $configFile)
{
    if (!file_exists($configFile)) {
        fwrite(STDERR, "Warning: unable to parse file $configFile.\n");
        return [];
    }
    $arr = parse_ini_file($configFile, true);
    if (!array_key_exists($section, $arr)) {
        fwrite(STDERR, "Warning: Section $section does not exist in file $configFile.\n");
        return [];
    }
    return $arr[$section]; // Array da secção
}


/**
 * Devolve as secções do ficheiro de configuração $configFile, com exepção das que pertencerem a $except.
 *
 * @since 0.4
 *
 * @param  string $configFile Path para o ficheiro de configuração.
 * @param  array $except      Secções que não devem ser devolvidas.
 * @return array              Array com as secções de $configFile, à excpeção das que estiverem contidas em $except.
 */
function get_sections_name($configFile, $except = [])
{
    if (!file_exists($configFile)) {
        fwrite(STDERR, "Warning: unable to parse file $configFile.\n");
        return [];
    }
    return array_diff(array_keys(parse_ini_file($configFile, true)), $except);
}


/**
 * Tendo por base o array $fileList (ordenado) com todos os ficheiros a processar e o array $afile com um ficheiro
 * processado, devolve array com todos os ficheiros em $fileList com timestamp superior ao ficheiro em $afile. Note-se
 * que os ficheiros estão de acordo com a expressão regular /^.+\.(\d+)\.json$/ (caso de $fileList) ou
 * /^.+\.(\d+)\.csv$/ (caso de $afile)
 *
 * @since 0.4
 *
 * @param  array $fileList  Lista ordenada de ficheiros a processar.
 * @param  array $afile     Lista com um ficheiro processado, no máximo.
 * @return mixed            False, caso $afile tenha mais do que um elemento, ou /^.+\.(\d+)\.json$/ não fizer match
 *                                 com os ficheiros em $fileList ou $afile. Caso contrário, devolve array com
 *                                 ficheiros a processar.
 */
function files2process($fileList, $afile)
{
    if (empty($fileList)) {
        return [];
    }
    if (empty($afile)) {
        return $fileList;
    }
    $prm = preg_match('/^.+\.(\d+)\.csv$/', $afile[0], $matches);
    if (count($afile) > 1 || # $afile não pode ter mais de um elemento
        !$prm) { # $afile tem de ter o formato indicado no preg_match anterior
        fwrite(STDERR, "Warning: Condition not matched - files2process ID:1 -.\n");
        return false;
    }
    $timestamp = $matches[1];
    $elem = 0;
    foreach ($fileList as $file) {
        $prm = preg_match('/^.+\.(\d+)\.json$/', $file, $matches);
        if (!$prm) {
            fwrite(STDERR, "Warning: Condition not matched - files2process ID:2 -.\n");
            return false;
        }
        $tst = $matches[1];
        if ($tst > $timestamp) {
            return array_slice($fileList, $elem);
        }
        $elem ++;
    }
    return [];
}


/**
 * Procura os ID de pesquisa diferentes na lista de ficheiros $fileList. Cada ficheiro
 * deve estar de acordo com a expressão regular $regexp.
 *
 * @since 0.4
 *
 * @param  array  $fileList Lista de ficheiros a processar.
 * @param  string $regexp   Expressão regular.
 * @return array            Lista de IDs de pesquisa únicos na $fileList.
 */
function searchIDs($fileList, $regexp = '/^.+\.(.+)\.\d+\.json$/')
{
    $listID = [];
    if (empty($fileList)) {
        return [];
    }
    foreach ($fileList as $file) {
        if (!preg_match($regexp, $file, $matches)) {
            fwrite(STDERR, "Warning: File $file doesn't match regexp - searchIDs -.\n");
        } else {
            $listID[] = $matches[1];
        }
    }
    return array_unique($listID);
}



/**
 * Cria diretoria
 *
 * Cria diretoria se não existir, devolvendo estado de sucesso da sua existência
 * (TRUE - diretoria existia ou foi criada; FALSE, no caso contrário).
 *
 * @since 0.3
 *
 * @param  string $dir  Diretoria a criar.
 * @return bool TRUE - diretoria existe (criada ou já existia); FALSE, caso contrário.
 */
function create_dir($dir)
{
    $path = realpath($dir);
    if ($path !== false && is_dir($path)) {
        return true;
    }
    if (!mkdir($dir, 0755, true)) {
        fwrite(STDERR, "Error: Directory $dir could not be created.\n");
        return false;
    }
    return true;
}


/**
 * Devolve o elemento do $arr associativo identificado por $elem. $elem tem o formato
 * 'aaa/bbb/ccc', significando que deve ser devolvido $arr['aaa']['bbb']['ccc'].
 *
 * @since 0.4
 *
 * @param  array    $arr   Array associativo multi-dimensional.
 * @param  string   $elem  Elemento do array a devolver, no formato "aa/bb/cc".
 * @return mixed    false, no caso do $arr não conter $elem. Caso contrário devolve o elemento do $arr
 *                         na posição $elem.
 */
function get_elem($arr, $elem)
{
    $ind = explode('/', $elem);
    for ($i=0; $i < count($ind); $i++) {
        if (array_key_exists($ind[$i], $arr)) {
            $arr = $arr[$ind[$i]];
        } else {
            fwrite(STDERR, "Warning: Index " . $ind[$i] . " (in $elem) doesn't exist - get_elem -.\n");
            return false;
        }
    }
    return $arr;
}




// Returns the text given as parameter, with contained URLs interpreted as HTML links
function str2link($text)
{
    return preg_replace('@((https?://)?([-\w]+\.[-\w\.]+)+\w(:\d+)?(/([-\w/_\.]*(\?\S+)?)?)*)@', "<a href='$1' target='_blank'>$1</a>", $text);
}


function str2hashtag($text)
{
    return preg_replace('@\#(\w+)@', "<a href='https://twitter.com/hashtag/$1' target='_blank'>#$1</a>", $text);
}

function str2twuser($text)
{
    return preg_replace('@\@(\w+)@', "<a href='https://twitter.com/$1' target='_blank'>@$1</a>", $text);
}
