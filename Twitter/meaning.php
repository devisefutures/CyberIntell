<?php
/**
* CyberIntell - feed e aplicação capaz de recolher dados e monitorizar/alertar
*              a ocorrência de dados/ameaças.
*
* meaning.php
*     Processa os dados dando-lhe significado, preparando para a apresentação.
*
* @link   https://gitlab.com/devisefutures/CyberIntell
*
* @author       José Pina Miranda - Devise Futures, Lda. (jose.miranda@devisefutures.com)
* @copyright    2017, Devise Futures Lda.
* @version      0.4
* @package      CyberIntell
* @subpackage   Twitter
* @since        0.4
* @license      https://gitlab.com/devisefutures/CyberIntell/blob/master/LICENSE.md  GNU General Public License v3.0
*/

# ----- config init -----
/**
  * @since 0.4
  * @var string CONFIG  Path para ficheiro de configurações.
  */
const CONFIG = "twitter.meaning.conf";
 /**
  * @since 0.4
  * @var string GENERAL_SECTION  Nome da secção geral na CONFIG
  */
const GENERAL_SECTION = "general";
  /**
   * @since 0.4
   * @var string OUTPUT_DIR Nome da diretoria de output na GENERAL_SECTION
   */
const OUTPUT_DIR = "output";
  /**
   * @since 0.4
   * @var string INPUT_DIR Nome da diretoria de input na GENERAL_SECTION
   */
const INPUT_DIR = "input";
/**
 * @since 0.6
 * @var string VERSION Versão da aplicação
 */
const VERSION = '1.1';
# ----- config end -----

/**
 * funções genéricas
 */
require_once('misc.php');
require_once('format_functions.php');


/**
 * Adiciona significado aos ficheiros processados anteriormente, de acordo com as secções
 * de configuração de meaning no ficheiro $config. Note-se que $gsection é uma secção
 * genérica de configuração em $config.
 *
 * @since 0.3
 *
 * @param  string $config   Path para o ficheiro de configuração. Default: CONFIG.
 * @param  string $gsection Secção genérica de configuração em $config. Default: GENERAL_SECTION.
 * @return bool  False no caso de erro; true no caso contrário.
 */
function meaning($config = CONFIG, $gsection = GENERAL_SECTION)
{
    $ageneral = get_conf_section($gsection, $config);
    if (empty($ageneral) || !array_key_exists(OUTPUT_DIR, $ageneral) || !array_key_exists(INPUT_DIR, $ageneral)) {
        fwrite(STDERR, "Warning: Section $gsection (in file $config) lacks proper configuration.\n");
        return false;
    } elseif (!create_dir($ageneral[OUTPUT_DIR]) || !create_dir($ageneral[INPUT_DIR])) {
        return false;
    }
    $sections = get_sections_name($config, [GENERAL_SECTION]);
    if (empty($sections)) {
        fwrite(STDERR, "Warning: Could not find search type in file $config.\n");
        return false;
    }
    foreach ($sections as $sec) {
        $alloutputfiles = [];
        // ver quais são os IDs de pesquisa e processar para cada um deles.
        foreach (searchIDs(glob($ageneral[INPUT_DIR].$sec.".*.csv"), '/^.+\.(.+)\.\d+\.csv$/') as $searchID) {
            $inputfile = glob($ageneral[INPUT_DIR].$sec.".".$searchID.".*.csv")[0];
            $outputfile = $ageneral[OUTPUT_DIR].$sec.".".$searchID.".csv";
            $alloutputfiles[] = $outputfile;
            $conf = get_conf_section($sec, $config);
            meaningSearchID($inputfile, $outputfile, $conf);
        }
        concat($alloutputfiles, $ageneral[OUTPUT_DIR].$sec.".csv");
    }
    return true;
}


/**
 * Concatena todos os ficheiros em $afilepath para o ficheiro $outfile, e faz o sort -u.
 *
 * @since 0.5
 *
 * @param  array $afilepath array com path para os ficheiros a concatenar.
 * @param  string $outfile   path para o ficheiro de destino da concatenação.
 * @return void
 */
function concat($afilepath, $outfile)
{
    $ind = 0;
    foreach ($afilepath as $file) {
        if (!$ind) { // se for o primeiro ficheiro
            `cat $file > $outfile`;
            $ind++;
            continue;
        }
        $lines = `wc -l $file` - 1;
        `tail -n $lines $file >> $outfile`;
    }
    $sortfile = "/tmp/usort." . time();
    `(head -n 1 $outfile && tail -n +2 $outfile | sort -u -k 3,3 -t ';' | sort -u -r) > $sortfile`;
    `head -n 401 $sortfile > $outfile.400`;
    `head -n 801 $sortfile > $outfile.800`;
    `head -n 2001 $sortfile > $outfile.2000`;
    `mv $sortfile $outfile`;
}


/**
 * Aplica as configurações $aconf (identificada no ficheiro de configuração de meaning) a
 * $ifile e escreve o output para $ofile.
 *
 * @since 0.4
 *
 * @param  string $ifile ficheiro de input (output da configuração de process)
 * @param  string $ofile ficheiro de output
 * @param  array $aconf array de regras (identificadas na configuração de meaning) a aplicar a $ifile
 * @return void
 */
function meaningSearchID($ifile, $ofile, $aconf)
{
    if (empty($aconf)) {
        fwrite(STDERR, "Warning: No rules for creating meaning for output file $ofile.\n");
    }
    $fpi = fopen($ifile, 'r');
    $csvheader = array_flip(str_replace(['"', "\n"], ['',''], explode(';', fgets($fpi))));
    $fpo = fopen($ofile, 'w');
    foreach ($aconf as $val) {
        fwrite($fpo, '"'.$val['header'].'";');
    }
    fwrite($fpo, "\n");
    while ($line = fgets($fpi)) {
        $row = explode(';', $line);
        foreach (array_keys($aconf) as $val) {
            fwrite($fpo, $aconf[$val]['func']($row, $csvheader, explode(';', $aconf[$val]['args'])).";");
        }
        fwrite($fpo, "\n");
    }
    fclose($fpi);
    fclose($fpo);
    $sortfile = "/tmp/mean." . time();
    `(head -n 1 $ofile && tail -n +2 $ofile | sort -u -k 3,3 -t ';' | sort -u -r) > $sortfile`;
    `head -n 401 $sortfile > $ofile.400`;
    `head -n 801 $sortfile > $ofile.800`;
    `head -n 2001 $sortfile > $ofile.2000`;
    `mv $sortfile $ofile`;
}


/**
 * Função de execução do programa
 * @param  string $options  Similar ao primeiro argumento de getopt().
 * @param  array $longopts  Similar ao segundo argumento de getopt().
 * @param  int $nparams     Número de argumentos obrigatórios da linha de comando + 1.
 * @param  array $arg   Argumentos da linha de comando (deverá ser $argv).
 * @return int
 */
function main($options, $longopts, $nparams, $arg)
{
    $aopt = getopt($options, $longopts);
    if (empty($aopt) && count($arg) > $nparams) {
        fwrite(STDOUT, "Usage: php $arg[0] [--h] [--v] [-f <config filename>]\n");
        return 0;
    } elseif (array_key_exists("h", $aopt)) {
        fwrite(STDOUT, "Usage: php $arg[0] [--h] [--v] [-f <config filename>]\n");
        fwrite(STDOUT, "   --h \t\thelp\n");
        fwrite(STDOUT, "   --v \t\tversion\n");
        fwrite(STDOUT, "   -f \t\tmeaning configuration file\n");
        fwrite(STDOUT, "\n$arg[0] - Gives meaning to previously processed data from Twitter, according to config file.\n\n");
        return 0;
    } elseif (array_key_exists("v", $aopt)) {
        fwrite(STDOUT, "$arg[0] \nVersion:" . VERSION. "\n");
        return 0;
    }
    (array_key_exists("f", $aopt)) ? meaning($aopt["f"]) : meaning();
    return 0;
}

exit(main("f:", ["h", "v"], 1, $argv));
