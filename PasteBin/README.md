# CyberIntell PasteBin - Manual do utilizador

## 1. CyberIntell PasteBin

Para avaliar a possibilidade de alavancar a utilização de uma [stack ELK][elk] para a indexação e visualização de OSINT, utilizamos o PasteBin como caso de uso.

O propósito desta aplicação é recolher os últimos *pastes*, e procurar no seu conteúdo por palavras pré-configuradas num ficheiro. Se forem encontradas, o *paste* é inserido no Elasticsearch, e a sua visualização é possível através do Kibana.

Esta aplicação é constituída pelos seguintes ficheiros:

 + `gather_pastes.php` - aplicação que recolhe os dados do PasteBin de acordo com as pesquisas de ameaças a efetuar, adiciona uma tag com as palavras-chaves que foram encontradas (*paste_matches*), e alimenta a base de dados Elasticsearch
 + `pastebin.search_terms.conf` - ficheiro onde devem ser configuradas as palavras-chave a procurar nos *pastes*
 +  `load_template.sh` - *script* que carrega no Elasticsearch os mapeamentos dos campos a indexar

[elk]: https://www.elastic.co/webinars/introduction-elk-stack "stack ELK"
[pg_cscd]: http://academiamilitar.pt/pos-graduacao-em-ciberseguranca-e-ciberdefesa.html "Pós-Graduação em Cibersegurança e Ciberdefesa"

## 2. Instalação

**Nota:** *Pressupõe a pré-instalação e acesso a uma [stack ELK][elk] no IP 127.0.0.1.*

Copie a última versão da branch [master](https://gitlab.com/devisefutures/CyberIntell/repository/archive.tar.gz?ref=master) para o seu computador.

### 2.1 Dependências

O código foi testado em ambiente Linux, com as seguintes versões de software:

 - **php**, **php-cli**, **php-common** e **php-xml**, versão 5.4.16 
 - **bash**, versão 4.2.46
 - **elasticsearch**, versão 5.2.2
 - **kibana**, versão 5.2.2

## 3. Configuração

 A configuração pressupõe que detém uma conta no PasteBin - se não tem conta, pode criar uma [aqui](http://pastebin.com/signup).
 
De seguida, e após ter efetuado o *login* no site do PasteBin, aceda à diretoria PasteBin e efetue as seguintes configurações:

 1. Editar ficheiro **pastebin.search_terms.conf** e inserir as palavras-chaves a pesquisar, uma por linha 
 2. Editar ficheiro **gather_pastes.php** e colocar a chave da API do PasteBin na variável ***$API_DEV_KEY*** - consulta a sua chave [aqui](http://pastebin.com/api#1).
 
## 4. Utilização

Para correr a aplicação, execute primeiro o seguinte comando para carregar os mapeamentos corretos num *template* Elasticsearch (isto permitirá criar visualizações mais ricas). Apenas precisa de correr este comando uma única vez.
```
bash load_template.sh
```


Para efetuar a recolha de dados do PasteBin, execute o seguinte comando:
```
php gather_pastes.php
```

A aplicação irá utilizar as palavras-chave definidas no ficheiro **pastebin.search_terms.conf**, e recolher todos os *pastes* que contenham as mesmas, inserindo de seguida no Elasticsearch, debaixo do índice **pastes**.
Se o *paste* já tiver sido inserido no Elasticsearch, não será recolhido novamente.

Este comando poderá ser colocado num *cron*, recolhendo a informação de forma periódica.

## 5. Consulta da informação 

De forma a consultar a informação recolhida, deverá aceder ao Kibana, normalmente disponível em http://127.0.0.1:5601/app/kibana.

Acedendo a *Management / Kibana / Indices* , deverá ser adicionado o índice **pastes**, escolhendo como campo de representação temporal, o campo com o nome **paste_date**.

### 5.1 Visualização
Na tab *"Discover"* poderá visualizar os *pastes* recolhidos, com a possibilidade de filtrar os dados pelos vários campos:

+  **paste_key** - identificador único do *paste*
+ **paste_date** - data / hora em que o *paste* foi criado
+  **paste_expire_date** - data/hora em que o *paste* expira (é apagado do PasteBin)
+  **paste_format_long** - formato (extenso) do conteúdo do *paste*, usado para *highlight* no site do PasteBin
+  **paste_format_short** - formato (abreviação) do conteúdo do *paste**, usado para *highlight* no site do PasteBin
+  **paste_hits** - número de visualizações do *paste*
+  **paste_matches** - palavras-chave detetadas no *paste*
+  **paste_private** - indicação se o *paste* é privado ou público (*booleano*)
+  **paste_size** - tamanho (em *bytes*) do *paste*
+  **paste_title** - título do *paste*
+  **paste_url** - link do *paste*
+  **paste_raw** - conteúdo do *paste*

### 5.2 Geração de inteligência
Alavancando as funcionalidades da [stack ELK][elk], é possível criar visualizações e *dashboards* que permitem gerar alguns dados sobre a informação recolhida:

 - O número total de *pastes* recolhidos 
 - O tamanho médio dos *pastes* 
 - O maior número de *hits* que um *paste* teve
 - As palavras-chave mais comuns
 - A evolução do número de pastes recolhidos ao longo do tempo
 - Etc.

## 6. Exemplos
Pode aceder a uma instância desta aplicação [aqui](https://elk.pedroqueiros.net/analytics), com os dados de autenticação que foram previamente partilhados.

Um exemplo de um *dashboard* construído com esta solução pode ser consultado [aqui](https://elk.pedroqueiros.net/analytics/goto/fb5c493f7cf1895aaec54369dc9fefa2?embed=true).



## 7. Análise crítica
A solução implementada permite a recolha de *pastes* que contenham uma ou mais palavras-chave pré-definidas, e a consequente consulta através de uma solução assente na [stack ELK][elk].

### 7.1 API PasteBin
No desenvolvimento da solução foram encontrados alguns problemas, principalmente relacionados com a API do PasteBin. A versão grátis da API do PasteBin tem algumas limitações, como por exemplo:

 1. Não permite qualquer tipo de filtragem de informação (a não ser a recolha dos *pastes* de um determinado utilizador) 
 2. Apenas permite recolher os 18 *trending* *pastes* atuais 
 3. Devolve a informação em formato XML (a versão paga devolve em JSON) 
 4. Não permite recolher a metainformação e a informação de um *paste* numa única chamada à API

A solução implementada colmata algumas das limitações encontradas acima, permitindo a recolha de toda a informação de um *paste* que contenha uma ou mais palavras-chave, adicionando a informação sobre as palavras-chave que foram encontradas no *paste* e convertendo a informação para JSON (com alguns *"hacks"* no caso de alguma metainformação estar vazia).
Também foi necessário ultrapassar alguns obstáculos no tratamento da informação, visto que a informação contida num *paste* pode ser de qualquer natureza e conter caractéres diversos que dificultam o *parsing* e catalogação da informação.

### 7.2 Stack ELK
A utilização da [stack ELK][elk] auxiliou no tratamento e visualização da informação, permitindo imediatamente a geração de alguma inteligência sobre os dados recolhidos.
No entanto, para a consulta dos dados em bruto, esta poderá não ser a melhor opção - como já foi referido, a natureza dos dados contidos em *pastes* é bastante heterogénea, e o GUI do Kibana não está adaptado para permitir a um operador explorar a informação de uma forma *user-friendly*.

Com o aumento do volume de informação recolhida, também poderão começar a existir constrangimentos ao nível do hardware *"low-cost"* utilizado (a solução apresentada como exemplo corre numa instância na *cloud*, com 2xCPU x86 2.40Ghz,  2GB de RAM e 50GB SSD). 


### 7.3 Melhorias a considerar

Existem, como é lógico, melhorias a fazer nesta aplicação, as quais são detalhadas (mas não estão restritas a) de seguida:

**Introdução de inteligência e capacidade de análise automática** - Permitir o correlacionamento de uma ou mais palavras chave e análise automática dos resultados obtidos, atribuíndo um nível de "interesse" que permita prioritizar a análise humana da informação considerada mais interessante.

**Integração com outra plataforma de consulta dos dados** - Possibilidade de integrar a informação recolhida com outras aplicações de ciber inteligência, como o [Minemeld](https://www.paloaltonetworks.com/products/secure-the-network/subscriptions/minemeld).

**Monitorização de *pastes* de um determinado utilizador** - Existindo a recolha de um *paste* considerado "interessante", poderia fazer sentido recolher o nome do utilizador que fez o *paste* e analisar todos os *pastes* desse utilizador.

**Alteração da catalogação das palavras chave** - Cada *paste* tem uma ou várias palavras-chave associadas. Da forma como foi implementada a catalogação (diretamente no elemento **paste**, num campo XML), não é ainda possível separar as mesmas para permitir a procura de forma individual via Kibana.
 
**Deteção e catalogação de padrões** - Seria interessante procurar por padrões de informação nos *pastes* que permitissem a catalogação dos mesmos (possível base de dados, lista de utilizadores/password, etc. - ver https://haveibeenpwned.com/Pastes).

**Alertas** - Utilizando ferramentas que já integram com o Elasticsearch (p.ex. [ElastAlert](https://github.com/Yelp/elastalert)), seria possível despoletar alarmes (e-mail, mensagem Slack, etc.) na deteção de algum padrão pré-definido.


## 8. Conclusão

Acreditamos que os utilizadores desta solução poderão rapidamente começar a recolher, de forma contínua, informações que sejam úteis e que estejam a ser partilhadas através do PasteBin.

Estas informações poderão ajudar as equipas de Ciber Inteligência a detetar e prevenir possíveis ciber ameaças, carecendo sempre, no entanto, da análise manual por parte de um Analista de Ciber Ameaças.
