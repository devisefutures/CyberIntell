#!/bin/bash

#
# Simple script to load PasteBin document template into Elasticsearch
#

curl -XPUT 'http://127.0.0.1:9200/_template/pastes?pretty' -H 'Content-Type: application/json' -d'
{
  "template": "pastes*",
  "settings": {
    "number_of_shards": 1
  },
  "mappings": {
      "paste" : {
        "properties" : {
          "paste_date" : {
            "type" : "date",
            "format" : "epoch_second"
          },
          "paste_expire_date" : {
            "type" : "date",
            "format" : "epoch_second"
          },
          "paste_format_long" : {
            "type" : "text",
			"fields" : {
              "keyword" : {
                "type" : "keyword",
                "ignore_above" : 256
              }
            }
          },
          "paste_format_short" : {
            "type" : "text",
            "fields" : {
              "keyword" : {
                "type" : "keyword",
                "ignore_above" : 256
              }
            }
          },
          "paste_hits" : {
            "type" : "integer"
          },
          "paste_key" : {
            "type" : "text",
			"fields" : {
              "keyword" : {
                "type" : "keyword",
                "ignore_above" : 256
              }
            }
          },
          "paste_matches" : {
            "type" : "text",
            "fields" : {
              "keyword" : {
                "type" : "keyword",
                "ignore_above" : 256
              }
            }
          },
          "paste_private" : {
            "type" : "boolean"
          },
          "paste_raw" : {
            "type" : "text",
            "fields" : {
              "keyword" : {
                "type" : "keyword",
                "ignore_above" : 256
              }
            }
          },
          "paste_size" : {
            "type" : "integer"
          },
          "paste_title" : {
            "type" : "text",
            "fields" : {
              "keyword" : {
                "type" : "keyword",
                "ignore_above" : 256
              }
            }
          },
          "paste_url" : {
            "type" : "text"
          }
        }
      }
    }
  }
'
