<?php
/**
* CyberIntell - feed e aplicação capaz de recolher dados e monitorizar/alertar
*              a ocorrência de dados/ameaças.
*
* gather_pastes.php
*     Recolhe dados do PasteBin que contêm keywords pré-definidas, converte para JSON e envia para Elasticsearch
*
* @link   https://gitlab.com/devisefutures/CyberIntell
*
* @author       Pedro Queirós (me@pedroqueiros.net)
* @copyright    2017
* @version      1.0
* @package      CyberIntell
* @subpackage   PasteBin
* @since        0.1
* @license      https://gitlab.com/devisefutures/CyberIntell/blob/master/LICENSE.md  GNU General Public License v3.0
*/


# ----- START Basic config -----

# PasteBin Developer Key - insert yours here!
$API_DEV_KEY = 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXX'; 

# ElasticSearch server URL
$ELASTIC_URL = 'http://127.0.0.1:9200/pastes/paste/?pretty';

###############################################################################################
## You shouldn't have to change anything below here, but you can customize it to your needs. ##
###############################################################################################

# Self explanatory - currently only displays your PasteBin DEV key
$DEBUG = 0;

# Relevant terms to search for
$CONF_FILE_LOCATION = 'pastebin.search_terms.conf';

# Paste keys that have been collected (i.e., stored in Elastic)
$COLLECTED_FILE_LOCATION = 'pastebin.collected_pastes.conf';

# PasteBin POST API URL
$API_POST_URL = 'http://pastebin.com/api/api_post.php';

# PasteBin RAW API URL
$API_RAW_URL = 'http://pastebin.com/api/api_raw.php';

# PasteBin LOGIN API URL
$API_LOGIN_URL = 'http://pastebin.com/api/api_login.php'; 


# ----- END Basic config -----


# ----- START Auxiliary functions -----

# Sets cURL options to send POST messages 
function set_curl_post_options($curl_handler) {
    curl_setopt($curl_handler, CURLOPT_POST, true); 
    curl_setopt($curl_handler, CURLOPT_RETURNTRANSFER, 1); 
    curl_setopt($curl_handler, CURLOPT_VERBOSE, 0); 
    curl_setopt($curl_handler, CURLOPT_NOBODY, 0); 
}

function set_curl_post_elastic($curl_handler) {
    curl_setopt($curl_handler, CURLOPT_POST, true); 
    curl_setopt($curl_handler, CURLOPT_HTTPHEADER,array('Content-Type: application/json')); 
    curl_setopt($curl_handler, CURLOPT_RETURNTRANSFER, 1); 
    curl_setopt($curl_handler, CURLOPT_VERBOSE, 0); 
    curl_setopt($curl_handler, CURLOPT_NOBODY, 0); 
}

# Given a paste key, retrieves the raw paste data
function get_raw_paste($paste_key) {
    $ch = curl_init('');
    curl_setopt($ch, CURLOPT_URL, 'http://pastebin.com/raw/'.$paste_key.''); 
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
    curl_setopt($ch, CURLOPT_VERBOSE, 0); 
    curl_setopt($ch, CURLOPT_NOBODY, 0); 

    $response = curl_exec($ch); 
    return $response;
}
# ----- END Auxiliary functions -----


# ----- START Main -----

if ($DEBUG == 1) {
    echo "API Dev Key: " . $API_DEV_KEY . "\n";
}

# Check to see we have all that is needed ...
if (!file_exists($CONF_FILE_LOCATION)) {
    echo "\nError: " . $CONF_FILE_LOCATION . " doesn't exist. Please create this file with the relevant search terms, one per line.\nExiting ...\n";
    return -1;
}

# First, let's get the 18 currently trending pastes (it's what you get for free...)
$ch = curl_init($API_POST_URL); 
$API_OPTION = 'trends';
curl_setopt($ch, CURLOPT_POSTFIELDS, 'api_option='.$API_OPTION.'&api_dev_key='.$API_DEV_KEY.''); 
set_curl_post_options($ch);

# Save the trending pastes in a correct XML structure
$response = '<pastes>';
$response .= curl_exec($ch); 
$response .= '</pastes>';


# Load the XML
$xml = simplexml_load_string($response);


# CONSIDER USING http://php.net/manual/en/function.preg-match.php TO SEARCH FOR PATTERNS (EMAILS, ETC).

# For each trending paste, lets add the raw paste data to it, if it matches our terms (filter results)
$SEARCH_TERMS = file($CONF_FILE_LOCATION);

foreach ($xml->paste as $paste) {
    $paste_raw = get_raw_paste($paste->paste_key);

    foreach ($SEARCH_TERMS as $term) {
        # Trim spaces and new lines in our search term - do we really want to do this?
        $term = trim($term);

        # Is this an existing paste? If yes, break
        if (file_exists($COLLECTED_FILE_LOCATION)) {
            $pattern = "/\b" . $paste->paste_key."\b/";
            $search = preg_grep($pattern, file($COLLECTED_FILE_LOCATION));
            if (!empty($search)) {
                break;
            }
        }

        # As this paste already been collected?
        # If yes, let's just update the terms list
        if (isset($paste->paste_raw)) {
            if (strpos($paste_raw, $term) !== false) {
	            $newTerms = $paste->paste_matches . ", " . $term;
                $paste->paste_matches = $newTerms;
            }
        }
        # This paste hasn't been collected, let's search for this new term
        else {
            # If it matches our term, let's save the raw paste and the matching term
            if (strpos($paste_raw, $term) !== false) {
                $paste->addChild("paste_raw", htmlspecialchars($paste_raw));
                $paste->addChild("paste_matches", $term);
            }
        }
    }
}

# Now, let's remove the pastes without matches - those that don't have a raw element
$dom = new DOMDocument();
$dom->preserveWhiteSpace = false; 
$dom->loadXML($xml->asXML());
$xpath = new DOMXPath($dom);
$query = sprintf('//paste[not(paste_raw)]');
foreach($xpath->query($query) as $paste) {
    $paste->parentNode->removeChild($paste);
}

# Save it!
$xml = $dom->saveXml();

# Finally, let's encode it to JSON and import it into Elastic
$xml = simplexml_load_string($xml);
foreach ($xml->paste as $paste) {
    # Encode it to JSON
    #$elk_paste = json_encode($paste, JSON_PRETTY_PRINT);
    $json = json_encode($paste);

    # Hack to convert empty JSON object to null string
    $elk_paste = str_replace(array('[]', '{}'), 'null', $json);
	
    # Send it to Elastic!
    $ch = curl_init($ELASTIC_URL);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $elk_paste);
    set_curl_post_elastic($ch);
    $response = curl_exec($ch);

    # If successfully sent to Elastic, save the paste key so we don't collect it again
    if (!curl_errno($ch)) {
        switch ($http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE)) {
            case 201:  # OK
                file_put_contents($COLLECTED_FILE_LOCATION, $paste->paste_key . "\n", FILE_APPEND | LOCK_EX);
                break;
            default:   # NOT OK
                echo "\nError submitting paste to Elastic. Please check the logs.\n\n";
            }
    }
}

return 0;
# ----- END Main -----

# My only friend, the end ...
?>
